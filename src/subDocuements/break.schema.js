const mongoose = require("mongoose");

const breakSchema = mongoose.Schema(
  {
    in: Date,
    out: Date,
    inCoord: {
      latitude: Number,
      longitude: Number,
    },
    inImage: String,
    outCoord: {
      latitude: Number,
      longitude: Number,
    },
    outImage: String,
    reason: String,
    inByAdmin: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    outByAdmin: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
  },
  { _id: false }
);

module.exports = breakSchema;
