const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { leavesValidation } = require("../../validations");
const { leavesController } = require("../../controllers");
const leavesService = require("../../services/leaves.service");
const router = express.Router();
router
  .route("/")
  .get(
    auth(),
    validate(leavesValidation.getLeaves),
    leavesController.getLeaves
  );

router.post(
  "/updateStatus",
  auth(),
  validate(leavesValidation.updateStatus),
  leavesController.updateStatus
);

router
  .route("/request")
  .get(
    auth(),
    validate(leavesValidation.getUserLeaves),
    leavesController.getUserLeaves
  )
  .post(
    auth(),
    validate(leavesValidation.createLeaves),
    leavesController.createLeaves
  );

router
  .route("/types")
  .get(auth(), validate(leavesValidation.getTypes), leavesController.getTypes)
  .post(
    auth(),
    validate(leavesValidation.createType),
    leavesController.createType
  )
  .patch(
    auth(),
    validate(leavesValidation.updateType),
    leavesController.updateType
  )
  .delete(
    auth(),
    validate(leavesValidation.deleteType),
    leavesController.deleteType
  );

router.get(
  "/leaveTotals",
  auth(),
  validate(leavesValidation.getLeaveTotals),
  leavesController.getLeaveTotals
);

module.exports = router;
