const express = require("express");
const authRoute = require("./auth.route");
const userRoute = require("./user.route");
const locationRoute = require("./location.route");
const performanceRoute = require("./performance.route");
const shiftRoute = require("./shift.route");
const timesheetRoute = require("./timesheet.route");
const answersRoute = require("./answers.route");
const companyRoute = require("./company.route");
const organizationRoute = require("./organization.route");
const complainRoute = require("./complain.route");
const activitiesRoute = require("./activities.route");
const healthRoute = require("./health.route");
const leavesRoute = require("./leaves.route");
const schedularRoute = require("./schedular.route");
const timerRoute = require("./timer.route");
const analyticsRoute = require("./analytics.route");
const customersRoute = require("./customers.route");

const router = express.Router();

const defaultRoutes = [
  {
    path: "/auth",
    route: authRoute,
  },
  {
    path: "/users",
    route: userRoute,
  },
  {
    path: "/location",
    route: locationRoute,
  },
  {
    path: "/performance",
    route: performanceRoute,
  },
  {
    path: "/shift",
    route: shiftRoute,
  },
  {
    path: "/schedular",
    route: schedularRoute,
  },
  {
    path: "/timesheet",
    route: timesheetRoute,
  },
  {
    path: "/answers",
    route: answersRoute,
  },
  {
    path: "/company",
    route: companyRoute,
  },
  {
    path: "/organization",
    route: organizationRoute,
  },

  {
    path: "/complain",
    route: complainRoute,
  },

  {
    path: "/activities",
    route: activitiesRoute,
  },

  {
    path: "/health",
    route: healthRoute,
  },
  {
    path: "/leaves",
    route: leavesRoute,
  },
  {
    path: "/timer",
    route: timerRoute,
  },
  {
    path: "/analytics",
    route: analyticsRoute,
  },
  {
    path: "/customers",
    route: customersRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;
