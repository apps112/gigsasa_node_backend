const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { timerValidation } = require("../../validations");
const { timerController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(timerValidation.createTimer),
    timerController.createTimer
  );

router.post(
  "/break",
  auth(),
  validate(timerValidation.saveBreak),
  timerController.saveBreak
);

router.post(
    "/breakout",
    auth(),
    validate(timerValidation.saveBreakout),
    timerController.saveBreakout
  );

module.exports = router;
