const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { shiftValidation } = require("../../validations");
const { shiftController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .post(
    auth(),
    validate(shiftValidation.createShift),
    shiftController.createShift
  );

router
  .route("/:shiftId")
  .patch(
    auth(),
    validate(shiftValidation.updateShift),
    shiftController.updateShift
  );

router.get(
  "/allocation",
  auth(),
  validate(shiftValidation.getAllocations),
  shiftController.getAllocations
);

router.get(
  "/allocation/user",
  auth(),
  validate(shiftValidation.getUserAllocations),
  shiftController.getUserAllocations
);

router.delete(
  "/allocation/:allocationId",
  auth(),
  validate(shiftValidation.deleteAllocation),
  shiftController.deleteAllocation
);

router.post(
  "/clockin",
  auth(),
  validate(shiftValidation.clockIn),
  shiftController.clockIn
);
router.post(
  "/clockout",
  auth(),
  validate(shiftValidation.clockOut),
  shiftController.clockOut
);
router.post(
  "/breakin",
  auth(),
  validate(shiftValidation.breakIn),
  shiftController.breakIn
);
router.post(
  "/breakout",
  auth(),
  validate(shiftValidation.breakOut),
  shiftController.breakOut
);

router.get(
  "/in-outs",
  auth(),
  validate(shiftValidation.getCurrentStatus),
  shiftController.getInOuts
);

router.get(
  "/todaysActive",
  auth(),
  validate(shiftValidation.getActivities),
  shiftController.getTodayActive
);

router.get(
  "/todayOnTime",
  auth(),
  validate(shiftValidation.getActivities),
  shiftController.getTodayOnTimes
);

router.get(
  "/weeklyActivity",
  auth(),
  validate(shiftValidation.getActivities),
  shiftController.getWeeklyActivity
);

router.get("/currentAllocation", auth(), shiftController.currentAllocation);

router.get(
  "/upcoming",
  auth(),
  validate(shiftValidation.getUpcoming),
  shiftController.getUpcoming
);

module.exports = router;
