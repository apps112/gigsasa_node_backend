const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { timesheetValidation } = require("../../validations");
const { timesheetController } = require("../../controllers");
const router = express.Router();

router
  .route("/")
  .post(
    auth(),
    validate(timesheetValidation.createTimesheet),
    timesheetController.createTimesheet
  )
  .get(
    auth(),
    validate(timesheetValidation.getTimesheets),
    timesheetController.getTimesheets
  )
  .delete(
    auth(),
    validate(timesheetValidation.deleteTimesheet),
    timesheetController.deleteTimesheet
  );

router.get(
  "/daily",
  auth(),
  validate(timesheetValidation.getDailyTimesheets),
  timesheetController.getDailyTimesheets
);

router.post(
  "/specific",
  auth(),
  validate(timesheetValidation.getSpecificTimesheets),
  timesheetController.getSpecificTimesheets
);

router.post(
  "/totalTimes",
  auth(),
  validate(timesheetValidation.getTimeSheetTotal),
  timesheetController.getTotalTimes
);

router.get(
  "/user",
  auth(),
  validate(timesheetValidation.getUserTimesheets),
  timesheetController.getUserTimesheets
);

router.get(
  "/getExportData",
  auth(),
  validate(timesheetValidation.getTimesheets),
  timesheetController.getTimeSheetDataForExport
);

router.post(
  "/editTime",
  auth(),
  validate(timesheetValidation.editTime),
  timesheetController.editTime
);

router.get(
  "/pending",
  auth(),
  validate(timesheetValidation.getPending),
  timesheetController.getPending
);

router.post(
  "/updateStatus",
  auth(),
  validate(timesheetValidation.updateStatus),
  timesheetController.updateStatus
);

router
  .route("/request")
  .post(
    auth(),
    validate(timesheetValidation.requestTimesheet),
    timesheetController.requestTimesheet
  )
  .get(
    auth(),
    validate(timesheetValidation.getRequests),
    timesheetController.getRequests
  )
  .patch(
    auth(),
    validate(timesheetValidation.updateRequest),
    timesheetController.updateRequest
  );

module.exports = router;
