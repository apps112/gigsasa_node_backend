const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { customersValidation } = require("../../validations");
const { customersController } = require("../../controllers");
const router = express.Router();

router
  .route("/")
  .get(
    auth(),
    validate(customersValidation.getCustomers),
    customersController.getCustomers
  )
  .post(
    auth(),
    validate(customersValidation.createCustomer),
    customersController.createCustomer
  );

router
  .route("/:customerId")
  .patch(
    auth(),
    validate(customersValidation.updateCustomer),
    customersController.updateCustomer
  );

router.get("/assignments", auth(), customersController.getUserCustomers);

router.get(
  "/specific",
  auth(),
  validate(customersValidation.getSpecificCustomers),
  customersController.getSpecificCustomers
);

module.exports = router;
