const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { organizationValidation } = require("../../validations");
const { organizationController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(organizationValidation.createOrganization),
    organizationController.createOrganization
  )
  .get(
    auth(),
    validate(organizationValidation.getOrganizations),
    organizationController.getOrganizations
  );
router
  .route("/:organizationId")
  .patch(
    auth(),
    validate(organizationValidation.updateOrganizations),
    organizationController.updateOrganization
  );

module.exports = router;
