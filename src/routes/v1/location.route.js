const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const locationValidation = require("../../validations/location.validation");
const locationController = require("../../controllers/location.controllers");

const router = express.Router();

router
  .route("/")
  .post(
    auth(),
    validate(locationValidation.createLocation),
    locationController.createLocation
  )
  .get(
    auth(),
    validate(locationValidation.getLocations),
    locationController.getLocations
  );

router
  .route("/:locationId")
  .patch(
    auth(),
    validate(locationValidation.updateLocation),
    locationController.updateLocation
  )
  .delete(
    auth(),
    validate(locationValidation.deleteLocation),
    locationController.deleteLocation
  );

module.exports = router;
