const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { answersValidation } = require("../../validations");
const { answersController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(answersValidation.createAnswers),
    answersController.createAnswers
  )
  .get(
    auth(),
    validate(answersValidation.getAnswers),
    answersController.getAnswers
  );

router
  .route("/allocation-answer")
  .get(
    auth(),
    validate(answersValidation.getAllocationAnswers),
    answersController.getAllocationAnswers
  );

module.exports = router;
