const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { analyticsValidation } = require("../../validations");
const { analyticsController } = require("../../controllers");
const router = express.Router();

router.get("/sampleDashboards", auth(), analyticsController.getSampleReports);
router.post(
  "/addDashboard",
  auth(),
  validate(analyticsValidation.createReport),
  analyticsController.addDashboard
);

router.patch(
  "/updatePowerBI",
  auth(),
  validate(analyticsValidation.updatePBI),
  analyticsController.updatePowerBiData
);

module.exports = router;
