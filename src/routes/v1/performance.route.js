const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { performanceValidation } = require("../../validations");
const { performanceController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(performanceValidation.createPerformance),
    performanceController.createPerformance
  )
  .get(
    auth(),
    validate(performanceValidation.getPerformance),
    performanceController.getPerformances
  );
router
  .route("/:performanceId")
  .patch(
    auth(),
    validate(performanceValidation.updatePerformance),
    performanceController.updatePerformance
  );

router.get(
  "/user",
  auth(),
  validate(performanceValidation.getUserPerformance),
  performanceController.getUserPerformance
);

module.exports = router;
