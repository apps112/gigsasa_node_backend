const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { healthValidation } = require("../../validations");
const { healthController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(healthValidation.createHealthEmergency),
    healthController.createHealthEmergency
  )
  .get(
    auth(),
    validate(healthValidation.getHealthEmergency),
    healthController.getHealthEmergencies
  );

module.exports = router;
