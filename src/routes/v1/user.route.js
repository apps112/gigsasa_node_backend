const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const userValidation = require("../../validations/user.validation");
const userController = require("../../controllers/user.controller");

const router = express.Router();

router.get(
  "/acceptJoin",
  validate(userValidation.joinUser),
  userController.acceptJoinReq
);
router
  .route("/")
  .post(auth(), validate(userValidation.createUser), userController.createUser)
  .get(auth(), validate(userValidation.getUsers), userController.getUsers);

router
  .route("/upload-many")
  .post(
    auth("createMultipleUsers"),
    validate(userValidation.createMultipleUsers),
    userController.createMultipleUsers
  );

router
  .route("/:userId")
  .get(auth(), validate(userValidation.getUser), userController.getUser)
  .patch(
    auth(),
    validate(userValidation.updateUser),
    userController.updateUser
  );

router.post(
  "/updateProfile",
  auth(),
  validate(userValidation.updateProfile),
  userController.updateProfile
);

router.post("/subsHook", userController.subscriptionCreated);
router.post("/subsCancel", userController.subscriptionCancelled);

router.post(
  "/joinRequest",
  auth(),
  validate(userValidation.joinRequest),
  userController.joinUserRequest
);

router.post(
  "/updateTourStatus",
  auth(),
  validate(userValidation.tourStatus),
  userController.updateTourStatus
);

module.exports = router;
