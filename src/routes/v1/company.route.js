const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { companyValidation } = require("../../validations");
const { companyController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth("createCompany"),
    validate(companyValidation.createCompany),
    companyController.createCompany
  )
  .get(
    auth(),
    validate(companyValidation.getCompany),
    companyController.getCompany
  );

router
  .route("/:companyId")
  .patch(
    auth(),
    validate(companyValidation.updateCompany),
    companyController.updateCompany
  );

module.exports = router;
