const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { complainValidation } = require("../../validations");
const { complainController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(complainValidation.createComplain),
    complainController.createComplain
  );

module.exports = router;
