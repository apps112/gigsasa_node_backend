const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { activitiesValidation } = require("../../validations");
const { activitiesController } = require("../../controllers");
const router = express.Router();

router
  .route("/")
  .get(
    auth(),
    validate(activitiesValidation.getActivities),
    activitiesController.getActivities
  );

module.exports = router;
