const express = require("express");
const validate = require("../../middlewares/validate");
const authValidation = require("../../validations/auth.validation");
const authController = require("../../controllers/auth.controller");
const auth = require("../../middlewares/auth");
const router = express.Router();
router.post(
  "/register",
  validate(authValidation.register),
  authController.register
);

router
  .route("/admin-signup")
  .post(validate(authValidation.registerAdmin), authController.registerAdmin);

router.post("/login", validate(authValidation.login), authController.login);
router.post("/logout", validate(authValidation.logout), authController.logout);
router.post(
  "/refresh-tokens",
  validate(authValidation.refreshTokens),
  authController.refreshTokens
);
router.post(
  "/forgot-password",
  validate(authValidation.forgotPassword),
  authController.forgotPassword
);
router.post(
  "/reset-password",
  validate(authValidation.resetPassword),
  authController.resetPassword
);

router.post(
  "/forgot-password-mobile",
  validate(authValidation.forgotPasswordMobile),
  authController.forgotPasswordMobile
);

router.post(
  "/reset-password-mobile",
  validate(authValidation.resetPasswordMobile),
  authController.resetPasswordMobile
);

router.post(
  "/update-password",
  auth(),
  validate(authValidation.updatePassword),
  authController.updatePassword
);

router.post(
  "/change-password",
  auth(),
  validate(authValidation.changePassword),
  authController.changePassword
);

router.post(
  "/set-image",
  auth(),
  validate(authValidation.setImage),
  authController.setImage
);

router.post(
  "/send-verification-email",
  auth(),
  authController.sendVerificationEmail
);
router.get(
  "/verify-email",
  validate(authValidation.verifyEmail),
  authController.verifyEmail
);
router.get(
  "/verify-user-email",
  validate(authValidation.verifyEmail),
  authController.verifyEmailUser
);
router.get("/current", auth(), authController.getCurrentUser);

module.exports = router;
