const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { schedularValidation } = require("../../validations");
const { schedularController } = require("../../controllers");
const router = express.Router();
router
  .route("/")
  .post(
    auth(),
    validate(schedularValidation.createSchedular),
    schedularController.createSchedular
  )
  .get(
    auth(),
    validate(schedularValidation.getSchedular),
    schedularController.getSchedular
  )
  .delete(
    auth(),
    validate(schedularValidation.deleteSchedular),
    schedularController.deleteSchedular
  );

router
  .route("/:schedularId")
  .patch(
    auth(),
    validate(schedularValidation.updateSchedular),
    schedularController.updateSchedular
  );

router.get(
  "/userSummary",
  auth(),
  schedularController.userSummary
);

module.exports = router;
