const allRoles = {
  user: [],
  supervisor: [],
  admin: ["getUsers", "manageUsers"],
  root: ["createCompany", "getUsers", "manageUsers", "createMultipleUsers"],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
