const tokenTypes = {
  ACCESS: "access",
  REFRESH: "refresh",
  RESET_PASSWORD: "resetPassword",
  VERIFY_EMAIL: "verifyEmail",
  JOIN_COMPANY: "joinCompany",
  JOIN_COMPANY_REQ: "joinCompanyRequest",
};

module.exports = {
  tokenTypes,
};
