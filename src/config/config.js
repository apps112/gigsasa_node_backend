const dotenv = require("dotenv");
const path = require("path");
const Joi = require("joi");

dotenv.config({ path: path.join(__dirname, "../../.env") });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string()
      .valid("production", "development", "test")
      .required(),
    PORT: Joi.number().default(3000),
    MONGODB_URL: Joi.string().required().description("Mongo DB url"),
    JWT_SECRET: Joi.string().required().description("JWT secret key"),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number()
      .default(30)
      .description("minutes after which access tokens expire"),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number()
      .default(30)
      .description("days after which refresh tokens expire"),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description("minutes after which reset password token expires"),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description("minutes after which verify email token expires"),
    SMTP_HOST: Joi.string().description("server that will send the emails"),
    SMTP_PORT: Joi.number().description("port to connect to the email server"),
    SMTP_USERNAME: Joi.string().description("username for email server"),
    SMTP_PASSWORD: Joi.string().description("password for email server"),
    EMAIL_FROM: Joi.string().description(
      "the from field in the emails sent by the app"
    ),
    SERVER_URL: Joi.string().description("the URL where is server is deployed"),
    CLIENT_URL: Joi.string().description(
      "the URL where the client is deployed"
    ),
    APP_ADMIN_MAIL: Joi.string().description("email for the app admin"),

    AUTHENTICATION_MODE: Joi.string().description(
      "powerbi authentication mode"
    ),
    SCOPE: Joi.string().description("powerbi scope"),
    PBI_CLIENT_ID: Joi.string().description("powerbi client id"),
    PBI_USERNAME: Joi.string().description("powerbi username"),
    PBI_PASSWORD: Joi.string().description("powerbi password"),
    PBI_CLIENT_SECRET: Joi.string().description("powerbi client secret"),
    TENANT_ID: Joi.string().description("powerbi tenant id"),
    AUTHORITY_URI: Joi.string().description("powerbi authority uri"),
    PBI_DEFAULT_WORKSPACE: Joi.string().description(
      "powerbi default workspace"
    ),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema
  .prefs({ errors: { label: "key" } })
  .validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongoose: {
    url: envVars.MONGODB_URL + (envVars.NODE_ENV === "test" ? "-test" : ""),
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    },
  },
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes:
      envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
    },
    from: envVars.EMAIL_FROM,
  },

  SERVER_URL: envVars.SERVER_URL,
  CLIENT_URL: envVars.CLIENT_URL,
  APP_ADMIN_MAIL: envVars.APP_ADMIN_MAIL,
  powerBI: {
    authenticationMode: envVars.AUTHENTICATION_MODE,
    scope: envVars.SCOPE,
    clientId: envVars.PBI_CLIENT_ID,
    pbiUsername: envVars.PBI_USERNAME,
    pbiPassword: envVars.PBI_PASSWORD,
    clientSecret: envVars.PBI_CLIENT_SECRET,
    tenantId: envVars.TENANT_ID,
    authorityUri: envVars.AUTHORITY_URI,
    defaultWorkspace: envVars.PBI_DEFAULT_WORKSPACE,
  },
};
