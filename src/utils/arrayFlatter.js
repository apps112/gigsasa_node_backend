const flatNestedMembers = (arr) => {
  let flatArr = [];

  const getNestedObj = (desc) => {
    desc.forEach((doc) => {
      flatArr = flatArr.concat(doc.members);
      if (doc.descendants && doc.descendants.length) {
        getNestedObj(doc.descendants);
      }
    });
  };
  getNestedObj(arr);
  let members = flatArr.flat(1);
  let membersIds = [...new Set(members)];
  console.log("child members are", membersIds);
  return membersIds;
};

const flatNestedOrganizations = (arr) => {
  let flatArr = [];
  const getNestedObj = (desc) => {
    desc.forEach((doc) => {
      flatArr = flatArr.concat(doc.descendants);
      if (doc.descendants && doc.descendants.length) {
        getNestedObj(doc.descendants);
      }
    });
  };
  getNestedObj(arr);
  let organizations = flatArr
    .flat(1)
    .map((item) => item._id)
    .concat([arr[0]._id]);
  let organizationsIds = [...new Set(organizations)];
  return organizationsIds;
};

module.exports = {
  flatNestedOrganizations,
  flatNestedMembers,
};
