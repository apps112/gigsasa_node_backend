const getAllocations = (data, shift) => {
  let allocations = [];
  let { users, days } = data;
  if (data.isRepeat) {
    days.map((date) => {
      // for each day create allocation for every user
      users.map((user) => {
        allocations.push({
          user,
          date,
          shift: shift.id,
          type: "shift",
        });
      });
    });
  } else {
    users.map((user) => {
      allocations.push({
        user,
        date: data.start,
        shift: shift.id,
        type: "shift",
      });
    });
  }
  return allocations;
};

module.exports = getAllocations;
