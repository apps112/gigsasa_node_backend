const moment = require("moment");
const { eachDayOfInterval } = require("date-fns");

const defaultCoords = {
  latitude: 0,
  longitude: 0,
};

const getBreaks = (breaks, i) => {
  let obj = {};
  breaks.map((item, i) => {
    if (item) {
      i = i + 1;
      obj["Break In Time " + i] = moment(item.in).format("DD-MM-YYYY hh:mm");
      obj["Break Out Time " + i] = moment(item.out).format("DD-MM-YYYY hh:mm");
      // obj["Break In Coordinates " + i] = item.inCoord
      //   ? Object.values(item.inCoord || defaultCoords)
      //       .join(", ")
      //       .slice(1)
      //   : "";
      // obj["Break Out Coordinates " + i] = item.outCoord
      //   ? Object.values(item.outCoord).join(", ").slice(1)
      //   : "";
      obj["Break Reason " + i] = item.reason;
      return;
    }
  });
  return obj;
};

const getPerformances = (performance) => {
  let obj = {};
  performance.map((item, i) => {
    let qType = item.question.questionType == "out" ? "Clock Out" : "Clock In";
    i = i + 1;
    obj[`(${qType}) Performance Question ${i}`] = item.question.question;
    obj[`(${qType}) Performance Answer ${i}`] = item.answer;
    return;
  });
  return obj;
};

module.exports.parseSheet = (data) => {
  let result = data.map((item) => {
    let shiftType = item.allocation.shift
      ? item.allocation.shift.name
      : "Free Shift";
    return {
      Date: item.date,
      Member: item.user.name,
      Email: item.user.email,
      "Job Title": item.user.jobType ? item.user.jobType : "Admin",
      Role: item.user.role == "user" ? "worker" : item.user.role,
      ...(item.user.organization && {
        Department: item.user.organization.name,
      }),
      "Shift Type": shiftType,
      "Clock In Time": item.allocation.clockin,
      "Clock Out Time": item.allocation.clockout,
      "Clock In Coordinates": item.allocation.clockinCoord,
      "Clock Out Coordinates": item.allocation.clockoutCoord,
      "Number Of Breaks": item.allocation.breaks.length,
      "Total Hours Worked": item.time,
      "Total Break Time": item.breakTime,
      "Over Time": 0,
      "Leave Days Taken": 0,
      ...(item.allocation.breaks.length > 0 &&
        getBreaks(item.allocation.breaks)),
      ...(item.performance.length > 0 && getPerformances(item.performance)),
      ...(item.allocation.type === "schedule" &&
        item.allocation.schedule && {
          "Work Schedule Start Time": item.allocation.schedule.start,
          "Work Schedule Finish Time": item.allocation.schedule.end,
          "Work Schedule": item.allocation.schedule.name,
        }),
    };
  });

  return result;
};

function getDD2DMS(dms, type) {
  var sign = 1,
    Abs = 0;
  var days, minutes, secounds, direction;

  if (dms < 0) {
    sign = -1;
  }
  Abs = Math.abs(Math.round(dms * 1000000));
  //Math.round is used to eliminate the small error caused by rounding in the computer:
  //e.g. 0.2 is not the same as 0.20000000000284
  //Error checks
  if (type == "lat" && Abs > 90 * 1000000) {
    //alert(" Degrees Latitude must be in the range of -90. to 90. ");
    return false;
  } else if (type == "lon" && Abs > 180 * 1000000) {
    //alert(" Degrees Longitude must be in the range of -180 to 180. ");
    return false;
  }

  days = Math.floor(Abs / 1000000);
  minutes = Math.floor((Abs / 1000000 - days) * 60);
  secounds = (
    (Math.floor(((Abs / 1000000 - days) * 60 - minutes) * 100000) * 60) /
    100000
  ).toFixed();
  days = days * sign;
  if (type == "lat") direction = days < 0 ? "S" : "N";
  if (type == "lon") direction = days < 0 ? "W" : "E";
  //else return value
  return days * sign + "º" + minutes + "'" + secounds + "''" + direction;
}

module.exports.parsePowerBI = (data) => {
  const parsedData = data.map((item) => {
    delete item.Email;
    delete item.Role;
    delete item["Total Hours Worked"];
    delete item["Total Break Time"];
    delete item["Over Time"];

    if (
      !item["Clock In Coordinates"] ||
      !item["Clock In Coordinates"].latitude ||
      !item["Clock Out Coordinates"] ||
      !item["Clock Out Coordinates"].latitude
    ) {
      return {
        ...item,
        "Clock In Coordinates": "",
        "Clock Out Coordinates": "",
      };
    }

    let clockInCords = getDD2DMS(item["Clock In Coordinates"].latitude, "lat");
    let clockOutCords = getDD2DMS(
      item["Clock Out Coordinates"].longitude,
      "lon"
    );

    return {
      ...item,
      "Clock In Coordinates": clockInCords,
      "Clock Out Coordinates": clockOutCords,
    };
  });

  return parsedData;
};

module.exports.getUserIds = (array) => {
  let uids = [];

  array.forEach((item) => {
    if (item.user && item.user._id) {
      uids.push(item.user._id);
    }
  });

  return uids;
};

const getFormat = (date) => {
  return moment(date).format("DD-MM-YYYY");
};

const getValues = (days, item) => {
  return days.map((day) => {
    const obj = {
      date: day,
      renderType: "leave",
      ...item.toObject(),
      id: item._id,
    };

    obj.user.id = obj.user._id;
    delete obj._id;
    delete obj.user._id;
    return obj;
  });
};

module.exports.parseLeaveTimeSheet = (timeSheet = [], leaves = [], params) => {
  // generate all leave dates
  const { start, end } = params;

  let leaveDates = [];
  leaves.map((item) => {
    const startDate = moment(item.start).isBefore(moment(start), "date")
      ? moment(start).toDate()
      : moment(item.start).toDate();

    const endDate = moment(item.end).isAfter(moment(start), "date")
      ? moment(end).toDate()
      : moment(item.end).toDate();

    const days = eachDayOfInterval({
      start: startDate,
      end: endDate,
    });

    let arr = getValues(days, item);
    leaveDates = leaveDates.concat(arr);
  });

  return { timeSheet, leaves: leaveDates };
};
