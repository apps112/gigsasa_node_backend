const isRootUser = (user) => {
  const isRoot = user.role === "root";
  return isRoot;
};

module.exports = { isRootUser };
