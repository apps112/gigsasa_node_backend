const adal = require("adal-node");
const config = require("../config/config");

const getAccessToken = async function () {
  // Use ADAL.js for authentication

  const AuthenticationContext = adal.AuthenticationContext;

  // Create a config variable that store credentials from config.json

  const authorityUrl = config.powerBI.authorityUri;

  // Check for the MasterUser Authentication
  if (config.powerBI.authenticationMode.toLowerCase() === "masteruser") {
    let context = new AuthenticationContext(authorityUrl);
    return new Promise((resolve, reject) => {
      context.acquireTokenWithUsernamePassword(
        config.powerBI.scope,
        config.powerBI.pbiUsername,
        config.powerBI.pbiPassword,
        config.powerBI.clientId,
        function (err, tokenResponse) {
          // Function returns error object in tokenResponse
          // Invalid Username will return empty tokenResponse, thus err is used
          if (err) {
            reject(tokenResponse == null ? err : tokenResponse);
          }
          resolve(tokenResponse);
        }
      );
    });

    // Service Principal auth is the recommended by Microsoft to achieve App Owns Data Power BI embedding
  } else if (
    config.powerBI.authenticationMode.toLowerCase() === "serviceprincipal"
  ) {
    authorityUrl = authorityUrl.replace("common", config.powerBI.tenantId);
    let context = new AuthenticationContext(authorityUrl);

    return new Promise((resolve, reject) => {
      context.acquireTokenWithClientCredentials(
        config.powerBI.scope,
        config.powerBI.clientId,
        config.powerBI.clientSecret,
        function (err, tokenResponse) {
          // Function returns error object in tokenResponse
          // Invalid Username will return empty tokenResponse, thus err is used
          if (err) {
            reject(tokenResponse == null ? err : tokenResponse);
          }
          resolve(tokenResponse);
        }
      );
    });
  }
};

function getAuthHeader(accessToken) {
  return "Bearer ".concat(accessToken);
}

async function getRequestHeader() {
  // Store authentication token
  let tokenResponse;

  // Store the error thrown while getting authentication token
  let errorResponse;

  // Get the response from the authentication request
  try {
    tokenResponse = await getAccessToken();
  } catch (err) {
    if (
      err.hasOwnProperty("error_description") &&
      err.hasOwnProperty("error")
    ) {
      errorResponse = err.error_description;
    } else {
      // Invalid PowerBI Username provided
      errorResponse = err.toString();
    }
    return {
      status: 401,
      error: errorResponse,
    };
  }

  // Extract AccessToken from the response
  const token = tokenResponse.accessToken;
  return {
    "Content-Type": "application/json",
    Authorization: getAuthHeader(token),
  };
}

module.exports = getRequestHeader;
