const moment = require("moment");
const { eachDayOfInterval } = require("date-fns");
const getDates = (start, end) => {
  console.log("start", start);
  console.log("end", end);
  let days = [];
  if (moment(end).isAfter(moment(start), "second")) {
    days = eachDayOfInterval({
      start: moment(start).toDate(),
      end: moment(end).toDate(),
    });
  } else {
    days = eachDayOfInterval({
      start: moment(end).toDate(),
      end: moment(start).toDate(),
    });
  }

  console.log("days", days);
  return days.map((day) => getFormat(day));
};

const getDifference = (start, end) => {
  let difference = new Date(end) - new Date(start);
  let minutes = Math.floor(difference / 1000 / 60);
  return minutes;
};

const getFormat = (date) => {
  return moment(date).format("YYYY-MM-DD");
};

const getBreakTime = (item, date) => {
  if (item) {
    if (item.in && item.out) {
      if (
        date == getFormat(item.in) &&
        getFormat(item.in) !== getFormat(item.out)
      ) {
        let time = getDifference(item.in, new Date(date).setHours(23, 59, 59));
        return time;
      } else if (
        date == getFormat(item.out) &&
        getFormat(item.in) !== getFormat(item.out)
      ) {
        let time = getDifference(new Date(date).setHours(00, 00, 00), item.out);
        return time;
      } else if (date == getFormat(item.in) && date == getFormat(item.out)) {
        let time = getDifference(item.in, item.out);
        return time;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

const getOverTime = (allocation, timeWorked) => {
  if (allocation.type == "shift" || allocation.type == "schedule") {
    let timeToBeWorked;
    switch (allocation.type) {
      case "shift":
        timeToBeWorked = getDifference(
          allocation.shift.start,
          allocation.shift.end
        );
        break;
      case "schedule":
        timeToBeWorked = getDifference(
          new Date("01/01/2007 " + allocation.schedule.start),
          new Date("01/01/2007 " + allocation.schedule.end)
        );
        break;
    }
    if (timeWorked > timeToBeWorked) {
      return timeWorked - timeToBeWorked;
    }
  }
  return 0;
};

const getTime = (allocation) => {
  let { clockin, clockout, breaks, id, user } = allocation;

  let result = [];
  let dates = getDates(clockin, clockout);
  dates.map((date) => {
    let breakTime = breaks
      .map((obj) => getBreakTime(obj, getFormat(date)))
      .reduce((a, b) => a + b, 0);
    let time;
    if (
      getFormat(date) !== getFormat(clockin) &&
      getFormat(date) !== getFormat(clockout)
    ) {
      // get breaks time
      time = 1440 - breakTime;
    } else if (
      getFormat(date) == getFormat(clockout) &&
      getFormat(clockin) !== getFormat(clockout)
    ) {
      time =
        getDifference(new Date(date).setHours(00, 00, 00), clockout) -
        breakTime;
    } else if (
      getFormat(date) == getFormat(clockin) &&
      getFormat(clockin) !== getFormat(clockout)
    ) {
      time =
        getDifference(clockin, new Date(date).setHours(23, 59, 59)) - breakTime;
    } else {
      time = getDifference(clockin, clockout) - breakTime;
    }
    result.push({
      time,
      breakTime,
      date: new Date(date),
      user: user,
      allocation: id,
    });
    return;
  });
  // calculate overTime here

  if (dates.length == 1 && result[0].time) {
    let overTime = getOverTime(allocation, result[0].time);
    result[result.length - 1].overTime = overTime;
  } else {
    // calculate for more than one days

    let time = result.reduce((help, current) => {
      help = help + current.time;
      return help;
    }, 0);
    let overTime = getOverTime(allocation, time);
    result[result.length - 1].overTime = overTime;
  }

  return result;
};

module.exports = { getTime, getBreakTime, getFormat };
