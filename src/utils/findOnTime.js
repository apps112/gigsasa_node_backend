const moment = require("moment");

const formatTime = (time) => moment(time).format("HH:mm");

const findOnTime = (allocations) => {
  const onTimes = [];
  const reliefMinutes = 5;

  allocations.forEach((item) => {
    const reliefTime = moment(formatTime(item.shift.start), "HH:mm").add(
      reliefMinutes,
      "minute"
    );

    const isOnTime = moment(formatTime(item.clockin), "HH:mm").isSameOrBefore(
      reliefTime,
      "minutes"
    );

    if (isOnTime) {
      onTimes.push(onTimes);
    }
  });
  return onTimes.length;
};

module.exports = findOnTime;
