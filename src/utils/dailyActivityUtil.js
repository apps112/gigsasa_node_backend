const getUserIds = (data) => {
  let userIds = [];
  if (data.results && data.results.length) {
    data.results.forEach((element) => {
      if (element.user && element.user._id) {
        userIds.push(element.user._id.toString());
      }
    });
  }
  return userIds;
};

const parseInOuts = ({ clockInResults, clockOutResults, breakInResults }) => {
  if (!clockOutResults.results || !clockOutResults.results.length) {
    return [clockInResults, clockOutResults, breakInResults];
  }

  let clockInUsers = getUserIds(clockInResults);

  let breakInUsers = getUserIds(breakInResults);

  let clockOutData = [];
  clockOutResults.results.forEach((item) => {
    if (
      item.user &&
      !clockInUsers.includes(item.user._id.toString()) &&
      !breakInUsers.includes(item.user._id.toString())
    ) {
      clockOutData.push(item);
    }
  });

  const parsedData = [
    clockInResults,
    { ...clockOutResults, results: clockOutData },
    breakInResults,
  ];

  return parsedData;
};

module.exports = { parseInOuts };
