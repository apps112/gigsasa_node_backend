const moment = require("moment");
const { getFormat, getTime } = require("./getTime");

const generateWeekDays = () => {
  const startOfWeek = moment().startOf("week");
  const endOfWeek = moment().endOf("week");

  const days = [];
  let day = startOfWeek;

  while (day <= endOfWeek) {
    days.push(day.toDate());
    day = day.clone().add(1, "d");
  }

  return days;
};

// const timeFormat = "HH:mm";
// const formatTime = (time) => moment(time).format(timeFormat);

// const getOverTime = (allocation) => {
//   const clockInDate = new Date(getFormat(allocation.clockin));
//   const shiftOutDate = new Date(allocation.shift.end).setYear(
//     clockInDate.getFullYear(),
//     clockInDate.getMonth(),
//     clockInDate.getDate()
//   );

//   const shiftTime = moment.utc(formatTime(allocation.shift.end), timeFormat);
//   const clockOutTime = moment.utc(formatTime(allocation.clockout), timeFormat);

//   if (clockOutTime.isAfter(shiftTime)) {
//     console.log("shiftTime", shiftTime.format(timeFormat));
//     console.log("clockoutTime", clockOutTime.format(timeFormat));
//     // overTime logic
//     console.log("overTimeDif", clockOutTime.diff(shiftTime));

//     const overTime = moment.duration(clockOutTime.diff(shiftTime));

//     console.log("overTime", moment.utc(+overTime).format("H:mm"));

//     return;
//   }

//   return { overTime: 0, clockout: allocation.clockout };
// };

const getWeekActivities = (allocation, weekDays) => {
  let dayTimes = [];

  let totalShiftTime = 0;
  let totalBreakTime = 0;

  weekDays.forEach((day) => {
    let currentDayAllocations = [];

    allocation.map((item) => {
      if (getFormat(day) === getFormat(item.clockout)) {
        // getOverTime(item);

        const allocationTimes = getTime(item);

        currentDayAllocations = currentDayAllocations.concat(allocationTimes);
      }
    });

    let dayTime = 0;
    let breakTime = 0;

    currentDayAllocations.forEach((item) => {
      dayTime += item.time;
      breakTime += item.breakTime;
    });
    totalShiftTime += dayTime;
    totalBreakTime += breakTime;

    dayTimes.push({
      dayTime,
      breakTime,
      overTime: 0,
      day: moment(day).format("ddd"),
    });
  });

  console.log("dayTimes", {
    dayTimes,
    totalShiftTime,
    totalBreakTime,
    totalOverTime: 0,
  });
  return {
    dayTimes,
    totalShiftTime,
    totalBreakTime,
    totalOverTime: 0,
  };
};

module.exports = { generateWeekDays, getWeekActivities };
