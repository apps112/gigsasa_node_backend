const { Complain } = require("../models");

const createComplain = async (data) => {
  const complain = await Complain.create(data);
  return complain;
};

module.exports = {
  createComplain,
};
