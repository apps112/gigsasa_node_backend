const { Timer } = require("../models");

const createTimer = async (data) => {
  const timer = await Timer.create(data);
  return timer;
};

const getTimer = async (allocation) => {
  let timer = await Timer.findOne({
    allocation,
  });
  return timer;
};

const saveBreak = async (allocation, data) => {
  let timer = await Timer.findOneAndUpdate(
    {
      allocation,
    },
    {
      ...data,
    },
    {
      new: true,
    }
  );
  return timer;
};

const saveBreakout = async (allocation, breaks) => {
  let timer = await Timer.findOneAndUpdate(
    {
      allocation,
    },
    {
      breaks,
    },
    {
      new: true,
    }
  );
  return timer;
};

module.exports = {
  createTimer,
  getTimer,
  saveBreak,
  saveBreakout,
};
