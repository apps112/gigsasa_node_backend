const httpStatus = require("http-status");
const { Location } = require("../models");
const ApiError = require("../utils/ApiError");

const createLocation = async (body) => {
  const location = await Location.create(body);
  return location;
};

const queryLocations = async (filters, options) => {
  const locations = await Location.paginate(filters, options);
  return locations;
};

const getAllLocations = async (filters) => {
  const locations = await Location.find(filters);
  return locations;
};

const getLocationById = async (id) => {
  return Location.findById(id);
};

const updateLocationById = async (locationId, updateBody) => {
  const location = await getLocationById(locationId);
  if (!location) {
    throw new ApiError(httpStatus.NOT_FOUND, "location not found");
  }
  Object.assign(location, updateBody);
  await location.save();
  return location;
};

const deleteLocation = async (locationId) => {
  await Location.findByIdAndDelete({
    _id: locationId,
  });
  return;
};

module.exports = {
  createLocation,
  queryLocations,
  getLocationById,
  updateLocationById,
  deleteLocation,
  getAllLocations,
};
