const nodemailer = require("nodemailer");
const config = require("../config/config");
const logger = require("../config/logger");
const resetPasswordTemplate = require("../../templates/mobileResetPassword");
const verifyEmailTemplate = require("../../templates/verifyEmail");
const resetPsTemplate = require("../../templates/resetPassword");
const createUserTemplate = require("../../templates/createNewUser");
const complainTemplate = require("../../templates/complain");
const joinComReq = require("../../templates/joinCompanyRequest");
const acceptJoinReq = require("../../templates/requestAccepted");
const inviteCoOwner = require("../../templates/inviteCo-owner");

const transport = nodemailer.createTransport(config.email.smtp);
/* istanbul ignore next */
if (config.env !== "test") {
  transport
    .verify()
    .then(() => logger.info("Connected to email server"))
    .catch(() =>
      logger.warn(
        "Unable to connect to email server. Make sure you have configured the SMTP options in .env"
      )
    );
}

const sendEmail = async (to, subject, text) => {
  const msg = { from: config.email.from, to, subject, text };
  await transport.sendMail(msg);
};

const sendEmailHTML = async (to, subject, html) => {
  const msg = { from: config.email.from, to, subject, html };
  await transport.sendMail(msg);
};

const sendResetPasswordEmail = async (to, token) => {
  const subject = "Reset password";
  // replace this url with the link to the reset password page of your front-end app
  const resetPasswordUrl = `${config.CLIENT_URL}auth/change-password/${token}`;
  const text = `Dear user,
To reset your password, click on this link: ${resetPasswordUrl}
If you did not request any password resets, then ignore this email.`;

  const html = resetPsTemplate(resetPasswordUrl);
  await sendEmailHTML(to, subject, html);
};

const sendResetPasswordEmailMobile = async (user, code) => {
  const subject = "Reset password";
  const html = resetPasswordTemplate(user, code);
  await sendEmailHTML(user.email, subject, html);
};

const sendVerificationEmail = async (to, token, name) => {
  const subject = "Email Verification";
  // replace this url with the link to the email verification page of your front-end app
  const verificationEmailUrl = `${config.SERVER_URL}v1/auth/verify-email?token=${token}`;

  const html = verifyEmailTemplate(to, verificationEmailUrl, name);
  await sendEmailHTML(to, subject, html);
};

const sendCompanyInvitationEmail = async (to, token, companyName, userName) => {
  const subject = `Request to Join ${companyName} Company`;
  // replace this url with the link to the reset password page of your front-end app
  const joinLink = `${config.CLIENT_URL}auth/adminSignup/${token}`;

  const html = inviteCoOwner(userName, joinLink);

  await sendEmailHTML(to, subject, html);
};

const sendComplainMail = async (params) => {
  const html = complainTemplate(params);

  await sendEmailHTML(config.APP_ADMIN_MAIL, params.subject, html);
};

const sendNewUserMail = async (user, token) => {
  const subject = `Complete Your Onboarding Process On Gigsasa App`;
  // replace this url with the link to the reset password page of your front-end app
  let joinLink = `${config.SERVER_URL}v1/auth/verify-email?token=${token}`;
  console.log("userRole", user.role);
  if (user.role === "user") {
    joinLink = `${config.SERVER_URL}v1/auth/verify-user-email?token=${token}`;
  }

  const html = createUserTemplate(joinLink, user);

  await sendEmailHTML(user.email, subject, html);
};

const sendJoinComReqMail = async (params) => {
  const joinLink = `${config.SERVER_URL}v1/users/acceptJoin?token=${params.token}`;

  const subject = `Company join request`;

  const html = joinComReq(params.user.name, joinLink);
  await sendEmailHTML(params.email, subject, html);
};

const sendSuccessJoined = async (params) => {
  const loginLink = `${config.CLIENT_URL}auth/login`;

  const subject = `Request Approved`;
  const text = `Your request to join company has been successfully approved. To login, please click the below link
  ${loginLink} `;

  const html = acceptJoinReq(loginLink);

  await sendEmailHTML(params.email, subject, html);
};

module.exports = {
  transport,
  sendEmail,
  sendResetPasswordEmail,
  sendResetPasswordEmailMobile,
  sendVerificationEmail,
  sendCompanyInvitationEmail,
  sendComplainMail,
  sendNewUserMail,
  sendJoinComReqMail,
  sendSuccessJoined,
};
