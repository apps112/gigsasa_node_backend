const httpStatus = require("http-status");
const { Leaves, LeaveTypes } = require("../models");
const ApiError = require("../utils/ApiError");

const createLeaves = async (data) => {
  const result = await Leaves.create(data);
  return result;
};

const createType = async (data) => {
  const result = await LeaveTypes.create(data);
  return result;
};

const updateType = async (id, data) => {
  const result = await LeaveTypes.findOneAndUpdate(
    {
      _id: id,
    },
    {
      ...data,
    }
  );
  return result;
};

const deleteType = async (id) => {
  const result = await LeaveTypes.findOneAndDelete({
    _id: id,
  });
  return result;
};

const queryLeaves = async (page, filters) => {
  let leaves = await Leaves.paginate(
    {
      user: {
        $in: filters,
      },
    },
    {
      page,
      limit: 10,
      populate: "user",
      sortBy: "createdAt:desc",
    }
  );
  return leaves.results;
};

const queryLeavesTotals = async (filters) => {
  let leaves = await Leaves.find({
    user: {
      $in: filters,
    },
  });

  const totalLeaves = leaves.length;
  const totalApproved = leaves.filter((item) => item.status === "approved")
    .length;

  const totalPending = leaves.filter((item) => item.status === "pending")
    .length;
  const totalRejected = leaves.filter((item) => item.status === "rejected")
    .length;

  const leaveTotals = {
    totalLeaves,
    totalPending,
    totalApproved,
    totalRejected,
  };
  return leaveTotals;
};

const getTypes = async (company) => {
  console.log("company is", company);
  let types = await LeaveTypes.find({
    company,
  }).sort({
    createdAt: -1,
  });

  return types;
};

const getAllLeaves = async (params, userIds) => {
  let { start, end } = params;
  let leaves = await Leaves.find({
    user: {
      $in: userIds,
    },
    $or: [
      {
        start: {
          $gte: new Date(new Date(start).setHours(00, 00, 00)),
          $lt: new Date(new Date(end).setHours(23, 59, 59)),
        },
      },
      {
        end: {
          $gte: new Date(new Date(start).setHours(00, 00, 00)),
          $lt: new Date(new Date(end).setHours(23, 59, 59)),
        },
      },
    ],
    status: "approved",
  }).populate("user");
  return leaves;
};

const getSpecificLeaves = async (params, id) => {
  let { start, end } = params;
  let leaves = await Leaves.find({
    user: id,
    $or: [
      {
        start: {
          $gte: new Date(new Date(start).setHours(00, 00, 00)),
          $lt: new Date(new Date(end).setHours(23, 59, 59)),
        },
      },
      {
        end: {
          $gte: new Date(new Date(start).setHours(00, 00, 00)),
          $lt: new Date(new Date(end).setHours(23, 59, 59)),
        },
      },
    ],
    status: "approved",
  }).populate("user");
  return leaves;
};

const queryUserLeaves = async (page, user) => {
  let leaves = await Leaves.paginate(
    {
      user: user.id,
    },
    {
      page,
      limit: 10,
      sortBy: "createdAt:desc",
    }
  );
  return leaves.results;
};

const updateStatus = async (id, updateBody) => {
  const leave = await Leaves.findById(id);
  if (!leave) {
    throw new ApiError(httpStatus.NOT_FOUND, "leave request not found");
  }
  Object.assign(leave, updateBody);
  await leave.save();
  return leave;
};

module.exports = {
  createLeaves,
  queryLeaves,
  queryUserLeaves,
  updateStatus,
  getTypes,
  getAllLeaves,
  createType,
  updateType,
  deleteType,
  getSpecificLeaves,
  queryLeavesTotals,
};
