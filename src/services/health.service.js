const { Health } = require("../models");

const createHealthEmergency = async (data) => {
  const healthEmergency = await Health.create(data);
  return healthEmergency;
};

const queryHealthEmergencies = async (filters, options) => {
  const healthEmergency = await Health.paginate(
    {
      user: { $in: filters },
    },
    {
      page: options.page,
      limit: options.limit,
      sortBy: "createdAt:desc",
      populate: "user",
    }
  );

  return healthEmergency;
};

module.exports = {
  createHealthEmergency,
  queryHealthEmergencies,
};
