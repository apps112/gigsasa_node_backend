const httpStatus = require("http-status");
const tokenService = require("./token.service");
const userService = require("./user.service");
const Token = require("../models/token.model");
const ApiError = require("../utils/ApiError");
const { tokenTypes } = require("../config/tokens");

const loginUserWithEmailAndPassword = async (email, password, isSocialAuth) => {
  const user = await userService.getUserByEmail(email);

  if (!user || (!isSocialAuth && !(await user.isPasswordMatch(password)))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Incorrect email or password");
  }
  return user;
};

const loginUserWithPhoneAndPassword = async (phone, password) => {
  const user = await userService.getUserByPhone(phone);
  if (!user || !(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Incorrect phone or password");
  }
  return user;
};

const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({
    token: refreshToken,
    type: tokenTypes.REFRESH,
    blacklisted: false,
  });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, "Not found");
  }
  await refreshTokenDoc.remove();
};

const refreshAuth = async (refreshToken) => {
  try {
    const refreshTokenDoc = await tokenService.verifyToken(
      refreshToken,
      tokenTypes.REFRESH
    );
    const user = await userService.getUserById(refreshTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await refreshTokenDoc.remove();
    return tokenService.generateAuthTokens(user);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Please authenticate");
  }
};

const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(
      resetPasswordToken,
      tokenTypes.RESET_PASSWORD
    );
    const user = await userService.getUserById(resetPasswordTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await userService.updateUserById(user.id, { password: newPassword });
    await Token.deleteMany({ user: user.id, type: tokenTypes.RESET_PASSWORD });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Password reset failed");
  }
};

const resetPasswordMobile = async (user, password) => {
  try {
    await userService.updateUserById(user.id, { password });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Password reset failed");
  }
};

const updatePassword = async (user, newPassword) => {
  console.log("current user is", user);
  try {
    let updatedUser = await userService.updateUserById(user.id, {
      password: newPassword,
      isInitialPassword: false,
    });
    return updatedUser;
  } catch (error) {
    console.log("error is here", error);
    throw new ApiError(httpStatus.UNAUTHORIZED, "Password update failed");
  }
};

const changePassword = async (user, body) => {
  let { oldPassword, password } = body;
  try {
    // check if old password is correct
    if (!(await user.isPasswordMatch(oldPassword))) {
      throw new ApiError(httpStatus.UNAUTHORIZED, "Incorrect old password");
    }
    let updatedUser = await userService.updateUserById(user.id, {
      password: password,
    });
    return updatedUser;
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Password change failed");
  }
};

const setProfile = async (user, profilePic) => {
  try {
    let updatedUser = await userService.updateUserById(user.id, {
      profilePic,
    });
    return updatedUser;
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Profile  update failed");
  }
};

const verifyEmail = async (verifyEmailToken) => {
  try {
    const verifyEmailTokenDoc = await tokenService.verifyToken(
      verifyEmailToken,
      tokenTypes.VERIFY_EMAIL
    );
    const user = await userService.getUserById(verifyEmailTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await Token.deleteMany({ user: user.id, type: tokenTypes.VERIFY_EMAIL });
    await userService.updateUserById(user.id, {
      isEmailVerified: true,
      active: true,
    });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Email verification failed");
  }
};

const verifyInviteCompany = async (inviteToken) => {
  try {
    const verifyEmailTokenDoc = await tokenService.verifyToken(
      inviteToken,
      tokenTypes.JOIN_COMPANY
    );
    console.log("verifyEmailTokenDoc", verifyEmailTokenDoc);

    return verifyEmailTokenDoc;
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Verify Join Request failed");
  }
};

module.exports = {
  loginUserWithEmailAndPassword,
  loginUserWithPhoneAndPassword,
  logout,
  refreshAuth,
  resetPassword,
  verifyEmail,
  updatePassword,
  setProfile,
  changePassword,
  resetPasswordMobile,
  verifyInviteCompany,
};
