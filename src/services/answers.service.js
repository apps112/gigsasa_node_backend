const httpStatus = require("http-status");
const { Answers } = require("../models");
const ApiError = require("../utils/ApiError");

const createAnswers = async (data, user) => {
  let answers = data.map((item) => ({ ...item, user: user.id }));
  const results = await Answers.insertMany(answers);
  return results;
};

const queryAnswers = async (id) => {
  let answers = await Answers.find({
    question: id,
  })
    .populate({
      path: "allocation",
      populate: {
        path: "shift",
      },
    })
    .populate("user");
  return answers;
};

const queryAllocationAnswers = async (id) => {
  let answers = await Answers.find({
    allocation: id,
  }).populate("question");
  return answers;
};

module.exports = {
  createAnswers,
  queryAnswers,
  queryAllocationAnswers,
};
