const httpStatus = require("http-status");
const { Shift, Allocation } = require("../models");
const schedularService = require("./schedular.service");
const timerService = require("./timer.service");
const pick = require("../utils/pick");
const getAllocations = require("../utils/getAllocations");
const ApiError = require("../utils/ApiError");
const findOnTime = require("../utils/findOnTime");

const {
  generateWeekDays,
  getWeekActivities,
} = require("../utils/weeklyActivityUtil");
const moment = require("moment");

const createShift = async (body) => {
  const shift = await Shift.create(body);
  return shift;
};

const createAllocations = async (body, shift) => {
  let allocations = getAllocations(body, shift);
  let result = await Allocation.insertMany(allocations);
  return result;
};

const createAllocation = async (data) => {
  let allocation = await Allocation.create(data);
  return allocation;
};

const updateAllocation = async (data, id) => {
  let allocation = await Allocation.findOneAndUpdate(
    {
      _id: id,
    },
    {
      ...data,
    },
    {
      new: true,
    }
  );
  return allocation;
};

const deleteAllocationByID = async (id) => {
  let result = await Allocation.findOneAndDelete({
    _id: id,
  });
  return result;
};

const queryAllocations = async (params, filters) => {
  let { start, end } = params;
  const filterObj = {
    user: { $in: filters },
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
  };
  const allocations = await Allocation.find(filterObj)
    .populate("user")
    .populate({
      path: "shift",
      populate: {
        path: "location",
      },
    });

  return allocations;
};

const queryUserAllocations = async (params, user) => {
  let { start, end } = params;
  const filterObj = {
    user: user.id,
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
  };
  const allocations = await Allocation.find(filterObj)
    .populate("user")
    .populate({
      path: "shift",
      populate: {
        path: "location",
      },
    });

  return allocations;
};

const queryCurrentAllocation = async (user) => {
  const start = moment().startOf("day").toDate();
  const end = moment(start).endOf("day").toDate();
  let current = await Allocation.findOne({
    user: user.id,
    status: {
      $in: ["going", "break"],
    },
  }).populate({
    path: "shift",
    populate: {
      path: "location",
    },
  });
  if (current) {
    let first = current.toJSON();
    let timer = await timerService.getTimer(current.id);

    let result = {
      ...first,
      ...(timer && { timer: timer.toJSON() }),
    };
    console.log("result is", result);

    return result;
  } else {
    // find schedular if any
    let schedule = await schedularService.getSchedularForCurrentDay(user);
    if (schedule) {
      let result = await Allocation.findOne({
        type: "schedule",
        status: "allocation",
        "schedule.name": { $eq: schedule.name },
      });
      if (result) {
        return result;
      } else {
        // create allocation for schedule and return
        let createOne = await Allocation.create({
          type: "schedule",
          schedule,
          date: new Date(),
          user: user.id,
        });
        return createOne;
      }
    }
    const allocations = await Allocation.find({
      user: user.id,
      date: {
        $gte: start,
        $lte: end,
      },
      status: {
        $ne: "completed",
      },
    })
      .sort({ date: 1 })
      .populate({
        path: "shift",
        populate: {
          path: "location",
        },
      })
      .limit(1);
    return allocations.length > 0 ? allocations[0] : null;
  }
};

const getUpcoming = async (user, page) => {
  let current = await queryCurrentAllocation(user);
  const start = moment().startOf("day").toDate();
  let allocations = await Allocation.paginate(
    {
      user: user.id,
      date: {
        $gte: start,
      },
      status: "allocation",
    },
    {
      page,
      limit: 10,
      populate: "shift,shift.location",
      sortBy: "date:asc",
    }
  );
  if (current) {
    // remove active shift from list
    return allocations.results.filter((item) => item.id !== current.id);
  } else {
    return allocations.results;
  }
};

const queryInOuts = async (depUsers, options) => {
  const { query, status, page, limit, createdAt } = options;
  const paginateOpt = { page, limit, createdAt, populate: "user" };
  const allocations = await Allocation.paginate(
    {
      user: { $in: depUsers },
      status: status,
      [query]: {
        $gte: new Date().setHours(00, 00, 00),
        $lt: new Date().setHours(23, 59, 59),
      },
    },
    paginateOpt
  );

  return allocations;
};

const queryTodayActives = async (depUsers) => {
  const allocations = await Allocation.countDocuments({
    user: { $in: depUsers },
    status: "going",
    clockin: {
      $gte: new Date().setHours(00, 00, 00),
      $lt: new Date().setHours(23, 59, 59),
    },
  });
  const todaysActive = ((allocations || 0) / depUsers.length) * 100;
  return { activeUsers: todaysActive.toFixed(2) };
};

const queryOnTime = async (depUsers) => {
  const allocations = await Allocation.find({
    user: { $in: depUsers },
    clockin: {
      $gte: new Date().setHours(00, 00, 00),
      $lt: new Date().setHours(23, 59, 59),
    },
    shift: { $exists: true },
  }).populate("shift");
  const onTimes = findOnTime(allocations);
  const onTimeUsers = ((onTimes || 0) / depUsers.length) * 100;
  return { onTimeUsers: onTimeUsers.toFixed(2) };
};

const getShiftById = async (id) => {
  return Shift.findById(id);
};

const updateShiftById = async (shiftId, updateBody) => {
  const shift = await getShiftById(shiftId);
  if (!shift) {
    throw new ApiError(httpStatus.NOT_FOUND, "Shift not found");
  }
  Object.assign(shift, updateBody);
  await shift.save();
  return shift;
};

const clockInAllocation = async (body, user) => {
  let { allocation, clockin, clockinCoord, clockInByAdmin, customer } = body;
  if (allocation) {
    let updatedDoc = await Allocation.findOneAndUpdate(
      {
        _id: allocation,
      },
      {
        status: "going",
        clockin,
        clockinCoord,
        ...(body.clockinImage && {
          clockinImage: body.clockinImage,
        }),
        ...(clockInByAdmin && {
          clockInByAdmin,
        }),
        ...(customer && { customer }),
      },
      { new: true }
    ).populate("shift");
    return updatedDoc;
  } else {
    // create new one etc free shift
    let allocation = await Allocation.create({
      clockin,
      clockinCoord,
      status: "going",
      user: user.id,
      date: clockin,
      type: "free",
      ...(body.clockinImage && {
        clockinImage: body.clockinImage,
      }),
      ...(clockInByAdmin && {
        clockInByAdmin,
      }),
      ...(customer && { customer }),
    });
    return allocation;
  }
};

const breakInAllocation = async (body) => {
  let { allocation } = body;
  let brk = pick(body, ["in", "inCoord", "inImage", "reason", "inByAdmin"]);
  let updatedDoc = await Allocation.findOneAndUpdate(
    {
      _id: allocation,
    },
    {
      status: "break",
      $push: {
        breaks: brk,
      },
    },
    { new: true }
  );
  return updatedDoc;
};

const breakOutAllocation = async (body) => {
  let { allocation } = body;
  let brkOut = pick(body, ["out", "outCoord", "outImage", "outByAdmin"]);
  console.log("date to be out is", brkOut);
  let result = await Allocation.findOne({
    _id: allocation,
  });

  const allocationObj = { ...result.toJSON() };
  let breaks = [...allocationObj.breaks]
    .filter((n) => n)
    .map((brk) => {
      if (!brk.out) {
        return {
          ...brk,
          ...brkOut,
        };
      }
    });
  result.breaks = breaks;
  result.status = "going";
  await result.save();
  return result;
};

const clockOutAllocation = async (body) => {
  let { allocation, clockout, clockoutCoord, clockOutByAdmin } = body;
  let updatedDoc = await Allocation.findOneAndUpdate(
    {
      _id: allocation,
    },
    {
      status: "completed",
      clockout,
      clockoutCoord,
      ...(body.clockoutImage && {
        clockoutImage: body.clockoutImage,
      }),
      ...(clockOutByAdmin && {
        clockOutByAdmin,
      }),
    },
    { new: true }
  ).populate("shift");
  return updatedDoc;
};

const queryWeeklyActivity = async (depUsers) => {
  const weekDays = generateWeekDays();
  const allocations = await Allocation.find({
    user: { $in: depUsers },
    clockout: {
      $gte: new Date(weekDays[0]).setHours(00, 00, 00),
      $lt: new Date(weekDays[weekDays.length - 1]).setHours(23, 59, 59),
    },
  }).populate("shift");

  const weekActivity = getWeekActivities(allocations, weekDays);

  return weekActivity;
};

const updateAllocationTime = async (
  allocation,
  clockin,
  clockout,
  activity
) => {
  let result = await Allocation.findOneAndUpdate(
    {
      _id: allocation,
    },
    {
      clockin,
      clockout,
      ...(activity && {
        customer: activity,
      }),
    },
    {
      new: true,
    }
  ).populate("shift");
  return result;
};

const deleteAllocation = async (locationId) => {
  await Allocation.findByIdAndDelete({
    _id: locationId,
  });
  return;
};

module.exports = {
  createShift,
  createAllocations,
  createAllocation,
  updateAllocation,
  queryAllocations,
  updateShiftById,
  clockInAllocation,
  breakInAllocation,
  breakOutAllocation,
  clockOutAllocation,
  queryInOuts,
  queryTodayActives,
  queryOnTime,
  queryWeeklyActivity,
  deleteAllocation,
  queryCurrentAllocation,
  getUpcoming,
  queryUserAllocations,
  updateAllocationTime,
  deleteAllocationByID
};
