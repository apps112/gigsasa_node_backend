const { Schedular } = require("../models");
const { getDay } = require("../utils/getDay");

const createSchedular = async (data) => {
  const response = await Schedular.create(data);
  return response;
};

const querySchedular = async (filters, options) => {
  const page = +options.page;
  const limit = +options.limit;
  const result = await Schedular.find({
    users: { $in: filters },
  })
    .sort({ createdAt: "desc" })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate("users");

  return result;
};

const getSchedularById = async (id) => {
  return Schedular.findById(id);
};

const findOnePopulate = async (id) => {
  let schedule = await Schedular.findOne({
    _id: id,
  }).populate("users");
  return schedule;
};

const updateSchedularById = async (schedularId, updateBody) => {
  const schedular = await getSchedularById(schedularId);
  if (!schedular) {
    throw new ApiError(httpStatus.NOT_FOUND, "Schedule not found");
  }
  Object.assign(schedular, updateBody);
  await schedular.save();
  return schedular;
};

const getSchedularForCurrentDay = async (user) => {
  let day = getDay(new Date());
  let schedular = await Schedular.findOne({
    users: {
      $in: user.id,
    },
    [day]: { $exists: true },
  }).sort("-createdAt");
  if (schedular) {
    let values = schedular[day].end.split(":");
    if (new Date().setHours(values[0], values[1], 00) > new Date())
      return {
        name: schedular.name,
        start: schedular[day].start,
        end: schedular[day].end,
      };
  }
};

const deleteById = async (id) => {
  let result = await Schedular.findOneAndDelete({
    _id: id,
  });
  return result;
};

const userSummary = async (user) => {
  let result = await Schedular.findOne({
    users: {
      $in: user.id,
    },
  }).sort("-createdAt");
  return result;
};

module.exports = {
  createSchedular,
  querySchedular,
  getSchedularById,
  updateSchedularById,
  findOnePopulate,
  deleteById,
  getSchedularForCurrentDay,
  userSummary,
};
