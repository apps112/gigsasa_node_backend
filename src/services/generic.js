const organizationService = require("./organization.service");
const {
  flatNestedMembers,
  flatNestedOrganizations,
} = require("../utils/arrayFlatter");

const getUserDescendants = async (user, dept = false) => {
  let { organization } = user;
  if (dept) {
    organization = dept;
  }
  // get all child members of this user
  let results = await organizationService.findOrganization(organization);
  if (!results) {
    return [];
  }
  let members = flatNestedMembers([results]);
  return members;
};

const getDeptDescendants = async (user) => {
  let { organization } = user;
  // get all child organizations of this user
  let results = await organizationService.findOrganization(organization);
  if (!results) {
    return [];
  }
  let organizations = flatNestedOrganizations([results]);
  return organizations;
};

module.exports = {
  getUserDescendants,
  getDeptDescendants,
};
