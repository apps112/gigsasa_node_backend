const jwt = require("jsonwebtoken");
const moment = require("moment");
const httpStatus = require("http-status");
const config = require("../config/config");
const userService = require("./user.service");
const { Token } = require("../models");
const ApiError = require("../utils/ApiError");
const { tokenTypes } = require("../config/tokens");

const generateToken = (
  userId,
  expires,
  type,
  secret = config.jwt.secret,
  requestee = null,
  companyId = null
) => {
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
    ...(requestee && { requesteeEmail: requestee.email }),
    ...(companyId && { companyId }),
  };
  return jwt.sign(payload, secret);
};

const saveToken = async (
  token,
  userId,
  expires,
  type,
  blacklisted = false,
  requesteeEmail,
  companyId
) => {
  const tokenDoc = await Token.create({
    token,
    user: userId,
    expires: expires.toDate(),
    type,
    blacklisted,
    requesteeEmail,
    companyId,
  });
  return tokenDoc;
};

const verifyToken = async (token, type) => {
  const payload = jwt.verify(token, config.jwt.secret);
  const tokenDoc = await Token.findOne({
    token,
    type,
    user: payload.sub,
    blacklisted: false,
  });
  if (!tokenDoc) {
    throw new Error("Token not found");
  }

  if (payload.requesteeEmail) {
    return {
      ...tokenDoc.toObject(),
      requesteeEmail: payload.requesteeEmail,
    };
  }

  return tokenDoc;
};

const generateAuthTokens = async (user) => {
  const accessTokenExpires = moment().add(
    config.jwt.accessExpirationMinutes,
    "minutes"
  );
  const accessToken = generateToken(
    user.id,
    accessTokenExpires,
    tokenTypes.ACCESS
  );

  const refreshTokenExpires = moment().add(
    config.jwt.refreshExpirationDays,
    "days"
  );
  const refreshToken = generateToken(
    user.id,
    refreshTokenExpires,
    tokenTypes.REFRESH
  );
  await saveToken(
    refreshToken,
    user.id,
    refreshTokenExpires,
    tokenTypes.REFRESH
  );

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toDate(),
    },
  };
};

const generateResetPasswordToken = async (email) => {
  const user = await userService.getUserByEmail(email);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "No users found with this email");
  }
  const expires = moment().add(
    config.jwt.resetPasswordExpirationMinutes,
    "minutes"
  );

  const resetPasswordToken = generateToken(
    user.id,
    expires,
    tokenTypes.RESET_PASSWORD
  );
  await saveToken(
    resetPasswordToken,
    user.id,
    expires,
    tokenTypes.RESET_PASSWORD
  );
  return resetPasswordToken;
};

const generateVerifyEmailToken = async (user) => {
  const expires = moment().add(config.jwt.verifyEmailExpirationMinutes, "days");
  const verifyEmailToken = generateToken(
    user.id,
    expires,
    tokenTypes.VERIFY_EMAIL
  );
  await saveToken(verifyEmailToken, user.id, expires, tokenTypes.VERIFY_EMAIL);
  return verifyEmailToken;
};

const generateJoinCompanyToken = async (companyId, user, email) => {
  const expires = moment().add(
    config.jwt.verifyEmailExpirationMinutes,
    "minutes"
  );
  const token = generateToken(
    user.id,
    expires,
    tokenTypes.JOIN_COMPANY,
    config.jwt.secret,
    email,
    companyId
  );
  await saveToken(
    token,
    user.id,
    expires,
    tokenTypes.JOIN_COMPANY,
    false,
    email,
    companyId
  );
  return token;
};

const generateJoinComRequestToken = async (user, requestee) => {
  const expires = moment().add(1, "day");
  const token = generateToken(
    user.id,
    expires,
    tokenTypes.JOIN_COMPANY_REQ,
    config.jwt.secret,
    requestee
  );
  await saveToken(token, user.id, expires, tokenTypes.JOIN_COMPANY_REQ);
  return token;
};

module.exports = {
  generateToken,
  saveToken,
  verifyToken,
  generateAuthTokens,
  generateResetPasswordToken,
  generateVerifyEmailToken,
  generateJoinCompanyToken,
  generateJoinComRequestToken,
};
