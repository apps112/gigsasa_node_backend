const httpStatus = require("http-status");
const { Company } = require("../models");

const createCompany = async (data) => {
  const company = await Company.create(data);
  return company;
};

const getCompany = async (id) => {
  let company = await Company.findById(id);
  return company;
};

const updateCompanyById = async (companyId, updateBody) => {
  const company = await getCompany(companyId);
  if (!company) {
    throw new ApiError(httpStatus.NOT_FOUND, "company not found");
  }
  Object.assign(company, updateBody);
  await company.save();
  return company;
};

module.exports = {
  createCompany,
  getCompany,
  updateCompanyById,
};
