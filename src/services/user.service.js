const httpStatus = require("http-status");
const { User } = require("../models");
const ApiError = require("../utils/ApiError");
const { isRootUser } = require("../utils/userType");
const organizationService = require("./organization.service");
const genericService = require("./generic");
const companyService = require("./company.service");
const emailService = require("./email.service");
const { tokenTypes } = require("../config/tokens");
const Token = require("../models/token.model");

const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  const user = await User.create(userBody);
  return user;
};

const addUserToDescendants = async (user, id) => {
  await User.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        descendants_users: user.id,
      },
    }
  );
  return;
};

const addMultipleToDescendants = async (users, id) => {
  await User.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        descendants_users: {
          $each: users,
        },
      },
    }
  );
  return;
};

const queryUsers = async (options, user, dept) => {
  let filters = {
    company_id: user.company_id,
    _id: { $ne: user.id },
    ...(dept && { organization: dept }),
  };
  if (!isRootUser(user)) {
    const descendants = await genericService.getUserDescendants(user, dept);
    filters = {
      _id: { $in: descendants },
    };
  }
  const users = await User.paginate(filters, {
    ...options,
    populate: "organization",
  });
  return users;
};

const getAllUsers = async (user, dept) => {
  let filters = {
    company_id: user.company_id,
    _id: { $ne: user.id },
    ...(dept && { organization: dept }),
  };
  if (!isRootUser(user)) {
    const descendants = await genericService.getUserDescendants(user, dept);
    filters = {
      _id: { $in: descendants },
    };
  }
  const users = await User.find(filters).sort({
    createdAt: "desc",
  });
  return users;
};

const getUserById = async (id) => {
  return User.findById(id);
};

const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

const getUserByPhone = async (phone) => {
  return User.findOne({ phone });
};

const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

const addOrgToDescendants = async (organization, id) => {
  await User.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        descendants_depts: organization.id,
      },
    }
  );
  return;
};

const createMultiple = async (usersBody) => {
  const users = await User.insertMany(usersBody);
  return users;
};

const getSupervisorAdmin = async (user) => {
  let result = await User.findOne({
    role: { $in: ["admin", "supervisor"] },
    organization: user.organization,
  });
  if (result) {
    return result;
  } else {
    let company = await companyService.getCompany(user.company_id);
    return company;
  }
};

const getComUsersIds = async (compId, dept = null) => {
  let filters = {
    company_id: compId,
  };
  if (dept) {
    filters = {
      company_id: compId,
      organization: dept,
    };
  }
  const results = await User.find(filters, { _id: 1 });
  const userIds = results.map((doc) => doc.id);
  return userIds;
};

const acceptJoin = async (verifyJoinToken) => {
  const userObject = await getUserByEmail(verifyJoinToken.requesteeEmail);
  // get user organization

  const organizationId = userObject.organization
    ? [userObject.organization]
    : await organizationService.getComOrganizationIds(userObject.company_id);

  if (!organizationId.length) {
    throw new Error("organization not found");
  }

  // update requestor

  const updateRequestor = updateUserById(verifyJoinToken.user, {
    role: "user",
    organization: organizationId[0],
    jobType: "",
    company_id: userObject.company_id,
  });

  // update requestee descendants
  const updateUser = addUserToDescendants(
    { id: verifyJoinToken.user },
    userObject.id
  );

  const deleteToken = Token.deleteMany({
    user: verifyJoinToken.user,
    type: tokenTypes.JOIN_COMPANY_REQ,
  });

  const [requestorObj] = await Promise.all([
    updateRequestor,
    updateUser,
    deleteToken,
  ]);

  // send success mail to requestor
  await emailService.sendSuccessJoined({ email: requestorObj.email });
};

module.exports = {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  getUserByPhone,
  addUserToDescendants,
  addOrgToDescendants,
  createMultiple,
  addMultipleToDescendants,
  getAllUsers,
  getSupervisorAdmin,
  getComUsersIds,
  acceptJoin,
};
