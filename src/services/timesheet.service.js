const httpStatus = require("http-status");
const mongoose = require("mongoose");
const { Timesheets, Answers } = require("../models");
const { createAllocation, updateAllocation } = require("./shift.service");
const ApiError = require("../utils/ApiError");
const moment = require("moment");
const { getTime } = require("../utils/getTime");
const { parseSheet } = require("../utils/parseSheet");

const createTimesheet = async (body, userId) => {
  let allocation = await createAllocation(body);
  let data = getTime(allocation);
  const result = await Timesheets.insertMany(
    data.map((obj) => ({ ...obj, ...(userId && { creator: userId }) }))
  );
  return result;
};

const requestTimesheet = async (body) => {
  let allocation = await createAllocation(body);
  let data = getTime(allocation);
  const result = await Timesheets.insertMany(
    data.map((obj) => ({ ...obj, status: "requested" }))
  );
  return result;
};

const updateRequest = async (body, query) => {
  let allocation = await updateAllocation(body, query.allocation);
  let data = getTime(allocation);
  const one = Timesheets.insertMany(
    data.map((obj) => ({ ...obj, status: "requested" }))
  );
  let two = Timesheets.deleteMany({
    _id: query.timesheets.map((id) => mongoose.Types.ObjectId(id)),
  });
  let results = await Promise.all([one, two]);
  return results[0];
};

const updateStatus = async (body) => {
  let { status, timesheets } = body;
  let result = await Timesheets.updateMany(
    {
      _id: {
        $in: timesheets.map((id) => mongoose.Types.ObjectId(id)),
      },
    },
    {
      status,
    }
  );

  return result;
};

const getRequests = async (page, user) => {
  let sheets = await Timesheets.paginate(
    {
      user: user.id,
      status: {
        $in: ["requested", "revision"],
      },
    },
    {
      page,
      limit: 10,
      sortBy: "date:asc",
      populate: "allocation",
    }
  );
  return sheets.results;
};

const updateTimesheet = async (allocation, timesheets, userId) => {
  let data = getTime(allocation);
  let one = Timesheets.insertMany(
    data.map((obj) => ({ ...obj, editor: userId }))
  );
  let two = Timesheets.deleteMany({
    _id: timesheets.map((id) => mongoose.Types.ObjectId(id)),
  });
  let results = await Promise.all([one, two]);
  return results[0];
};

const deleteTimesheet = async (timesheets) => {
  let result = await Timesheets.deleteMany({
    _id: timesheets.map((id) => mongoose.Types.ObjectId(id)),
  });

  return result;
};

const getExportData = async (filters, params) => {
  let { start, end } = params;
  let result = [];
  let cursor = Timesheets.find({
    user: { $in: filters },
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
  })
    .populate({
      path: "allocation",
      populate: {
        path: "shift",
        populate: "location",
      },
    })
    .populate({ path: "user", populate: "organization" })
    .cursor();

  for (let doc = await cursor.next(); doc != null; doc = await cursor.next()) {
    let answers = await Answers.find({
      allocation: doc.allocation._id,
    }).populate("question");
    result.push({
      ...doc._doc,
      performance: answers,
    });
  }
  let sheet = parseSheet(result);

  return sheet;
};

const queryTimesheets = async (filters, params) => {
  let { start, end } = params;
  let limit = params.page ? 20 : 0;
  let page = params.page ? params.page : 1;
  const timesheets = await Timesheets.find({
    user: { $in: filters },
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
    status: {
      $nin: ["requested", "revision"],
    },
  })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate({
      path: "allocation",
      populate: [{ path: "shift" }, { path: "customer" }],
    })
    .populate("user")
    .populate("editor")
    .populate("creator");

  return timesheets;
};

const queryDailyTimesheets = async (filters, params) => {
  const start = moment().startOf("day").toDate();
  const end = moment(start).endOf("day").toDate();
  let { page } = params;
  let limit = 20;
  const timesheets = await Timesheets.find({
    user: { $in: filters },
    date: {
      $gte: start,
      $lte: end,
    },
    status: {
      $nin: ["requested", "revision"],
    },
  })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate({
      path: "allocation",
      populate: [{ path: "shift" }, { path: "customer" }],
    })
    .populate("user")
    .populate("editor")
    .populate("creator");
  return timesheets;
};

const getPendingTimesheets = async (filters, params) => {
  let { page } = params;
  let limit = 20;
  const timesheets = await Timesheets.find({
    user: { $in: filters },
    status: {
      $in: ["requested", "revision"],
    },
  })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate({
      path: "allocation",
      populate: [{ path: "shift" }, { path: "customer" }],
    })
    .populate("user")
    .populate("editor");
  return timesheets;
};

const queryUserTimesheets = async (user, params) => {
  let { start, end } = params;
  const timesheets = await Timesheets.find({
    user: user.id,
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
    status: {
      $nin: ["requested", "revision"],
    },
  }).populate({
    path: "allocation",
    populate: [{ path: "shift" }, { path: "customer" }],
  });

  return timesheets;
};

const querySpecificTimesheets = async (id, params) => {
  let { start, end } = params;
  let limit = params.page ? 20 : 0;
  let page = params.page ? params.page : 1;
  const timesheets = await Timesheets.find({
    user: id,
    date: {
      $gte: new Date(new Date(start).setHours(00, 00, 00)),
      $lt: new Date(new Date(end).setHours(23, 59, 59)),
    },
    status: {
      $nin: ["requested", "revision"],
    },
  })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate({
      path: "allocation",
      populate: [{ path: "shift" }, { path: "customer" }],
    })
    .populate("user")
    .populate("editor");

  return timesheets;
};

const aggregateTimes = async (id, params) => {
  const { start, end } = params;

  const totalTimes = await Timesheets.aggregate([
    {
      $match: {
        user: mongoose.Types.ObjectId(id),
        date: {
          $gte: new Date(new Date(start).setHours(00, 00, 00)),
          $lt: new Date(new Date(end).setHours(23, 59, 59)),
        },
        status: {
          $nin: ["requested", "revision"],
        },
      },
    },
    {
      $group: {
        _id: null,
        totalWorkedTime: { $sum: "$time" },
        totalBreakTime: { $sum: "$breakTime" },
        totalOverTime: { $sum: "$overTime" },
      },
    },
  ]);

  if (!totalTimes.length) {
    return null;
  }
  return totalTimes[0];
};

const aggregateTimesDaily = async (filters, params) => {
  const { start, end, queryType } = params;

  const s = moment().startOf("day").toDate();
  const e = moment(start).endOf("day").toDate();

  let match = {
    user: { $in: filters },
    status: {
      $nin: ["requested", "revision"],
    },
  };

  if (queryType === "daily") {
    match = {
      ...match,
      date: {
        $gte: s,
        $lte: e,
      },
    };
  } else {
    match = {
      ...match,
      date: {
        $gte: new Date(new Date(start).setHours(00, 00, 00)),
        $lt: new Date(new Date(end).setHours(23, 59, 59)),
      },
    };
  }

  const totalTimes = await Timesheets.aggregate([
    {
      $match: match,
    },
    {
      $group: {
        _id: null,
        totalWorkedTime: { $sum: "$time" },
        totalBreakTime: { $sum: "$breakTime" },
        totalOverTime: { $sum: "$overTime" },
      },
    },
  ]);

  console.log("totalTimesfds", totalTimes);

  if (!totalTimes.length) {
    return null;
  }
  return totalTimes[0];
};

module.exports = {
  createTimesheet,
  requestTimesheet,
  updateRequest,
  getRequests,
  queryTimesheets,
  queryUserTimesheets,
  queryDailyTimesheets,
  querySpecificTimesheets,
  aggregateTimes,
  aggregateTimesDaily,
  getExportData,
  updateTimesheet,
  getPendingTimesheets,
  updateStatus,
  deleteTimesheet,
};
