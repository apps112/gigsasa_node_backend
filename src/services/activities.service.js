const httpStatus = require("http-status");
const { Activities } = require("../models");
const ApiError = require("../utils/ApiError");

const createActivity = async (body, type, byAuthorizedAdmin) => {
  let { shift, _id, user } = body;
  let activity = {
    shift,
    allocation: _id,
    user,
    type,

    ...(byAuthorizedAdmin && { byAuthorizedAdmin }),
  };

  const getActivity = await Activities.create(activity);
  return getActivity;
};

const queryActivities = async (filters, options) => {
  const page = +options.page;
  const limit = +options.limit;

  const activities = await Activities.paginate(
    {
      user: { $in: filters },
    },
    {
      page,
      limit,
      populate: "user,shift,allocation,byAuthorizedAdmin",
      sortBy: "createdAt:desc",
    }
  );

  return activities;
};

module.exports = {
  createActivity,
  queryActivities,
};
