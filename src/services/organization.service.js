const httpStatus = require("http-status");
const { Organization } = require("../models");
const ApiError = require("../utils/ApiError");

const createOrganization = async (body) => {
  let result = await Organization.create(body);
  let organization = await findOnePopulate(result.id);
  return organization;
};

const findOnePopulate = async (id) => {
  let organization = await Organization.findOne({
    _id: id,
  }).populate("parent");
  return organization;
};

const addOrgToDescendants = async (id, organization) => {
  await Organization.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        descendants: organization.id,
      },
    }
  );
  return;
};

const deleteDescendantsFromParent = async (id) => {
  let result = await Organization.findOneAndUpdate(
    {
      descendants: id,
    },
    {
      $pull: {
        descendants: id,
      },
    }
  );
  console.log("result update is", result);
  return;
};

const queryOrganizations = async (orgIDs, options) => {
  const { limit, page } = options;
  const organizations = await Organization.find({ _id: { $in: orgIDs } })
    .sort({
      createdAt: "desc",
    })
    .populate("parent")
    .limit(limit)
    .skip(limit * (page - 1));
  return organizations;
};

const getAllOrganizations = async (orgIDs) => {
  const organizations = await Organization.find({ _id: { $in: orgIDs } }).sort({
    createdAt: "desc",
  });

  return organizations;
};

const getOrganizationById = async (id) => {
  return Organization.findById(id);
};

const updateOrganizationById = async (organizationId, updateBody) => {
  const organization = await getOrganizationById(organizationId);
  if (!organization) {
    throw new ApiError(httpStatus.NOT_FOUND, "Organization not found");
  }
  Object.assign(organization, updateBody);
  await organization.save();
  return organization;
};

const getComOrganizationIds = async (compId) => {
  const results = await Organization.find(
    {
      companyId: compId,
    },
    { _id: 1 }
  );

  const orgIds = results.map((doc) => doc.id);
  return orgIds;
};

const getComOrgRandom = async (compId) => {
  const results = await Organization.find({
    companyId: compId,
  }).limit(1);

  return results;
};

const addJobTypeToOrganization = async (id, jobType) => {
  let organization = await getOrganizationById(id);
  if (organization.jobTypes.includes(jobType)) {
    return true;
  } else {
    organization.jobTypes = [...organization.jobTypes, jobType];
    await organization.save();
    return true;
  }
};

const addMemberToOrganization = async (id, user) => {
  await Organization.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        members: user.id,
      },
    }
  );
  return;
};

const findOrganization = async (id) => {
  let organization = await Organization.findOne(
    {
      _id: id,
    },
    {
      members: 1,
      descendants: 1,
    }
  );
  return organization;
};

module.exports = {
  queryOrganizations,
  getOrganizationById,
  updateOrganizationById,
  createOrganization,
  getAllOrganizations,
  getComOrganizationIds,
  getComOrgRandom,
  addJobTypeToOrganization,
  addOrgToDescendants,
  findOrganization,
  addMemberToOrganization,
  deleteDescendantsFromParent,
};
