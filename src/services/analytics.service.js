const axios = require("axios");
const getRequestHeader = require("../utils/powerBiAuthtication");
const config = require("../config/config");

const instance = async (options) => {
  const headers = await getRequestHeader();

  const client = axios.create({
    baseURL: "https://api.powerbi.com/v1.0",
    headers,
  });

  const onSuccess = (response) => {
    return response.data;
  };

  const onError = (error) => {
    return Promise.reject(error.response ? error.response.data : error);
  };

  return client(options).then(onSuccess).catch(onError);
};

const getDefaultReports = () => {
  return instance({
    method: "GET",
    url: `/myorg/groups/${config.powerBI.defaultWorkspace}/reports`,
  });
};
const getDefaultDatasets = () => {
  return instance({
    method: "GET",
    url: `/myorg/groups/${config.powerBI.defaultWorkspace}/datasets`,
  });
};

const getTokenInGroup = async ({ groupId, data, dashboardId }) => {
  const getTokenApi = `https://api.powerbi.com/v1.0/myorg/groups/${groupId}/dashboards/${dashboardId}/GenerateToken`;
  const headers = await getRequestHeader();

  return axios.post(getTokenApi, data, {
    headers,
  });
};

const getToken = (data) => {
  return instance({
    method: "POST",
    url: `/myorg/GenerateToken`,
    data,
  });
};

const createWorkSpace = async (data) => {
  return instance({
    method: "POST",
    url: `/myorg/groups`,
    data,
  });
};

const createDataset = async (groupId, data) => {
  return instance({
    method: "POST",
    url: `/myorg/groups/${groupId}/datasets`,
    data,
  });
};

const createRows = async ({ groupId, data, datasetId, tableName }) => {
  return instance({
    method: "POST",
    url: `/myorg/groups/${groupId}/datasets/${datasetId}/tables/${tableName}/rows`,
    data,
  });
};

const cloneReport = async ({ reportId, data }) => {
  return instance({
    method: "POST",
    url: `/myorg/reports/${reportId}/Clone`,
    data,
  });
};

const deleteRows = ({ groupId, datasetId, tableName }) => {
  return instance({
    method: "DELETE",
    url: `/myorg/groups/${groupId}/datasets/${datasetId}/tables/${tableName}/rows`,
  });
};

module.exports = {
  getDefaultReports,
  getToken,
  getDefaultDatasets,
  createWorkSpace,
  createDataset,
  createRows,
  cloneReport,
  deleteRows,
};
