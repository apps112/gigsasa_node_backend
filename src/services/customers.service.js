const httpStatus = require("http-status");
const { Customer } = require("../models");
const ApiError = require("../utils/ApiError");

const createCustomer = async (body) => {
  const customer = await Customer.create(body);
  return customer;
};

const queryCustomers = async (filters, options) => {
  const customers = await Customer.paginate(
    { departments: { $in: filters } },
    { ...options, populate: "departments" }
  );

  return customers;
};

const getUserCustomers = async (organization) => {
  let customers = await Customer.find({
    departments: organization,
  });
  return customers;
};

const getAllCustomers = async (filters) => {
  const customers = await Customer.find({
    departments: { $in: filters },
  }).populate("departments");
  return customers;
};

const getCustomerById = async (id) => {
  return Customer.findById(id);
};

const updateCustomer = async (id, updateBody) => {
  const customer = await getCustomerById(id);
  if (!customer) {
    throw new ApiError(httpStatus.NOT_FOUND, "customer not found");
  }
  Object.assign(customer, updateBody);
  await customer.save();
  return customer;
};

module.exports = {
  createCustomer,
  queryCustomers,
  getAllCustomers,
  updateCustomer,
  getUserCustomers,
};
