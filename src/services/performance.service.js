const httpStatus = require("http-status");
const { Performance } = require("../models");
const ApiError = require("../utils/ApiError");

const createPerformance = async (body) => {
  const results = await Performance.create(body);
  return results;
};

const queryPerformances = async (filters, options, dept) => {
  const page = +options.page;
  const limit = +options.limit;

  let filterObj = {
    organization: { $in: filters },
  };

  if (dept) {
    filterObj = {
      organization: dept,
    };
  }

  console.log("filterObj", dept);

  const performances = await Performance.find(filterObj)
    .sort({ createdAt: "desc" })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate("organization");

  return performances;
};

const queryUserPerformances = async (user, type) => {
  const performances = await Performance.find({
    jobType: user.jobType,
    organization: user.organization,
    questionType: type,
  });

  console.log("my performances are", performances);

  return performances;
};

const getPerformanceById = async (id) => {
  return Performance.findById(id);
};

const updatePerformanceById = async (performanceId, updateBody) => {
  const performance = await getPerformanceById(performanceId);
  if (!performance) {
    throw new ApiError(httpStatus.NOT_FOUND, "Performance question not found");
  }
  Object.assign(performance, updateBody);
  await performance.save();
  return performance;
};

module.exports = {
  queryPerformances,
  getPerformanceById,
  updatePerformanceById,
  createPerformance,
  queryUserPerformances,
};
