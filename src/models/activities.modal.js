const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const activitiesSchema = mongoose.Schema(
  {
    type: {
      type: String,
      required: true,
    },
    shift: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Shift",
    },
    allocation: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Allocation",
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    byAuthorizedAdmin: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);
activitiesSchema.plugin(toJSON);
activitiesSchema.plugin(paginate);

const Activities = mongoose.model("Activities", activitiesSchema);

module.exports = Activities;
