const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const { toJSON, paginate } = require("./plugins");
const { roles } = require("../config/roles");

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email");
        }
      },
    },
    phone: {
      type: String,
      trim: true,
      unique: true,
      sparse: true,
      required: false,
    },
    password: {
      type: String,
      trim: true,
      minlength: 8,
      validate(value) {
        if (
          (!this.isInitialPassword || !this.isSocialAuth) &&
          (!value.match(/\d/) || !value.match(/[a-zA-Z]/))
        ) {
          throw new Error(
            "Password must contain at least one letter and one number"
          );
        }
      },
      private: true,
    },
    role: {
      type: String,
      enum: roles,
      default: "root",
    },
    jobType: {
      type: String,
    },
    organization: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Organization",
    },
    level: {
      type: Number,
      default: 0,
    },
    descendants_users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    descendants_depts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Organization",
      },
    ],
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
    NHIF: {
      type: String,
    },
    NSSF: {
      type: String,
    },
    taxDoc: {
      type: String,
    },
    nationalID: {
      type: String,
    },
    payRate: {
      type: Number,
    },
    isSocialAuth: {
      type: Boolean,
    },
    company_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
    active: {
      type: Boolean,
    },
    profilePic: {
      type: String,
    },
    isInitialPassword: {
      type: Boolean,
    },
    subscription_plan: {
      plan: String,
      start: Date,
      end: Date,
    },
    tourStatus: {
      type: String,
      default: "pending",
    },

    powerBi: {
      workspaceId: String,
      datasetId: String,
      reportId: String,
      embedUrl: String,
      
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre("validate", async function (next) {
  const user = this;

  if (!user.isSocialAuth && user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = mongoose.model("User", userSchema);

module.exports = User;
