const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const performanceSchema = mongoose.Schema(
  {
    question: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      enum: ["number", "options", "text"],
      default: "text",
    },
    options: [
      {
        type: String,
      },
    ],
    organization: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Organization",
    },
    jobType: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true,
    },
    questionType: {
      type: String,
      enum: ["in", "out"],
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
performanceSchema.plugin(toJSON);
performanceSchema.plugin(paginate);

const Performance = mongoose.model("Performance", performanceSchema);

module.exports = Performance;
