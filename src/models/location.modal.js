const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const locationSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    coordinates: {
      latitude: Number,
      longitude: Number,
    },
    shifts: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Shift",
      },
    ],
    radius: {
      type: Number,
      default: 0,
    },
    company: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Company",
      required: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
locationSchema.plugin(toJSON);
locationSchema.plugin(paginate);

const Location = mongoose.model("Location", locationSchema);

module.exports = Location;
