const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const shiftSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    users: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
        required: true,
      },
    ],
    start: {
      type: Date,
      required: true,
    },
    end: {
      type: Date,
      required: true,
    },
    location: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Location",
    },
    isRepeat: {
      type: Boolean,
      default: false,
    },
    repeatUntil: {
      type: Date,
    },
    days: [
      {
        type: Date,
      },
    ],

    minimumHours: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
shiftSchema.plugin(toJSON);

const Shift = mongoose.model("Shift", shiftSchema);

module.exports = Shift;
