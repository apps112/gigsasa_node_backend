const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const healthSchema = mongoose.Schema(
  {
    status: {
      type: String,
      required: true,
      enum: ["safe", "unsafe"],
    },

    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);
healthSchema.plugin(toJSON);
healthSchema.plugin(paginate);

const Health = mongoose.model("health", healthSchema);

module.exports = Health;
