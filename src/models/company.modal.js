const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const companySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    team_size: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    industry: {
      type: String,
      required: true,
    },
    performance_management: {
      type: Boolean,
      default: false,
    },
    payroll: {
      type: Boolean,
      default: false,
    },
    image_recognition: {
      type: Boolean,
      default: false,
    },
    leave_management: {
      type: Boolean,
      default: false,
    },
    attendance: {
      type: Boolean,
      default: false,
    },
    invite_email: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);
companySchema.plugin(toJSON);

const Company = mongoose.model("Company", companySchema);

module.exports = Company;
