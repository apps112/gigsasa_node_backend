const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const typesSchema = mongoose.Schema(
  {
    name: String,
    company: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Company",
    },
  },

  {
    timestamps: true,
  }
);

typesSchema.plugin(toJSON);

const LeaveTypes = mongoose.model("LeaveTypes", typesSchema);

module.exports = LeaveTypes;
