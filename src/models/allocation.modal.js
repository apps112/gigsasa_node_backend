const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");
const breakSchema = require("../subDocuements/break.schema");

const allocationSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
    clockin: {
      type: Date,
    },
    clockout: {
      type: Date,
    },
    clockinImage: {
      type: String,
    },
    clockoutImage: {
      type: String,
    },
    clockinCoord: {
      latitude: Number,
      longitude: Number,
    },
    clockoutCoord: {
      latitude: Number,
      longitude: Number,
    },
    breaks: [breakSchema],
    shift: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Shift",
    },
    status: {
      type: String,
      default: "allocation",
    },
    date: {
      type: Date,
    },
    clockInByAdmin: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    clockOutByAdmin: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    type: {
      type: String,
      enum: ["free", "shift", "schedule"],
    },
    schedule: {
      name: String,
      start: String,
      end: String,
    },
    customer: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "customers",
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
allocationSchema.plugin(toJSON);
allocationSchema.plugin(paginate);

const Allocation = mongoose.model("Allocation", allocationSchema);
module.exports = Allocation;
