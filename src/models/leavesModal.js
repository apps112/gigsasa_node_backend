const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const leavesSchema = mongoose.Schema(
  {
    start: {
      type: Date,
      required: true,
    },
    end: {
      type: Date,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    leavePeriod: {
      type: String,
      enum: ["fullDay", "halfDay"],
      default: "fullDay",
    },
    description: {
      type: String,
    },
    status: {
      type: String,
      enum: ["pending", "approved", "rejected"],
      default: "approved",
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    paid: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);
leavesSchema.plugin(toJSON);
leavesSchema.plugin(paginate);

const Leaves = mongoose.model("Leaves", leavesSchema);

module.exports = Leaves;
