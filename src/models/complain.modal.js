const mongoose = require("mongoose");
const validator = require("validator");
const { toJSON } = require("./plugins");

const complainSchema = mongoose.Schema(
  {
    comment: {
      type: String,
      required: true,
    },

    email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email");
        }
      },
    },
    name: {
      type: String,
      required: true,
    },
    subject: {
      type: String,
      required: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

complainSchema.plugin(toJSON);

const Complain = mongoose.model("Complain", complainSchema);

module.exports = Complain;
