const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const organizationSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    members: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
      },
    ],
    jobTypes: [{ type: String, default: [] }],
    numberOfMembers: {
      type: Number,
    },
    descendants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Organization",
      },
    ],
    parent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
organizationSchema.plugin(toJSON);
organizationSchema.plugin(paginate);

function autoPopulateSubs(next) {
  this.populate("descendants");
  next();
}

organizationSchema
  .pre("find", autoPopulateSubs)
  .pre("findOne", autoPopulateSubs);

const Organization = mongoose.model("Organization", organizationSchema);

module.exports = Organization;
