const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const timesheetSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
    date: {
      type: Date,
      required: true,
    },
    time: {
      type: Number,
    },
    breakTime: {
      type: Number,
    },
    overTime: {
      type: Number,
      default: 0,
    },
    allocation: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Allocation",
    },
    shift: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Shift",
    },
    editor: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    creator: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    status: {
      type: String,
      enum: ["requested", "approved", "rejected", "revision"],
      default: "approved",
    },
  },
  {
    timestamps: true,
  }
);
timesheetSchema.plugin(toJSON);
timesheetSchema.plugin(paginate);

const Timesheet = mongoose.model("Timesheet", timesheetSchema);

module.exports = Timesheet;
