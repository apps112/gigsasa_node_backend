const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const schedularSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    users: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
        required: true,
      },
    ],
    creator: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    },
    company: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Company",
    },
    monday: {
      start: String,
      end: String,
    },
    tuesday: {
      start: String,
      end: String,
    },
    wednesday: {
      start: String,
      end: String,
    },
    thursday: {
      start: String,
      end: String,
    },
    friday: {
      start: String,
      end: String,
    },
    saturday: {
      start: String,
      end: String,
    },
    sunday: {
      start: String,
      end: String,
    },
  },
  {
    timestamps: true,
  }
);
schedularSchema.plugin(toJSON);

const Schedular = mongoose.model("Schedular", schedularSchema);

module.exports = Schedular;
