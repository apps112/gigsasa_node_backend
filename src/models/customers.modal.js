const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const customersSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },

    departments: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Organization",
      },
    ],

    createdBy: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Company",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
customersSchema.plugin(toJSON);
customersSchema.plugin(paginate);

const customers = mongoose.model("customers", customersSchema);

module.exports = customers;
