const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const answersSchema = mongoose.Schema(
  {
    answer: {
      type: String,
      required: true,
    },
    question: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Performance",
    },
    allocation: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Allocation",
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
    }
  },
  {
    timestamps: true,
  }
);
answersSchema.plugin(toJSON);

const Answers = mongoose.model("Answers", answersSchema);

module.exports = Answers;
