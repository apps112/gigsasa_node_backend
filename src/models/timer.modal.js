const mongoose = require("mongoose");
const { toJSON } = require("./plugins");

const timerSchema = mongoose.Schema(
  {
    inTime: {
      type: Date,
    },
    breakTime: {
      type: Date,
      default: Date.now(),
    },
    breaks: {
      type: Number,
      default: 0,
    },
    counter: {
      type: String,
      default: "0:0:0",
    },
    allocation: {
      type: mongoose.Types.ObjectId,
      ref: "Allocation",
    },
  },
  {
    timestamps: true,
  }
);
timerSchema.plugin(toJSON);

const Timer = mongoose.model("Timer", timerSchema);

module.exports = Timer;
