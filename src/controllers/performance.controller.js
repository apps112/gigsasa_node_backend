const httpStatus = require("http-status");
const pick = require("../utils/pick");
const catchAsync = require("../utils/catchAsync");
const {
  performanceService,
  organizationService,
  userService,
  genericService,
} = require("../services");
const { isRootUser } = require("../utils/userType");

const createPerformance = catchAsync(async (req, res) => {
  let { organization, jobType } = req.body;
  const one = performanceService.createPerformance(req.body);
  // add jobType to organization
  const two = organizationService.addJobTypeToOrganization(
    organization,
    jobType
  );
  let results = await Promise.all([one, two]);
  let performance = results[0];
  res.status(httpStatus.CREATED).send(performance);
});

const getPerformances = catchAsync(async (req, res) => {
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const user = req.user;
  const filters = isRootUser(user)
    ? await organizationService.getComOrganizationIds(user.company_id)
    : await genericService.getDeptDescendants(user);

  const result = await performanceService.queryPerformances(
    filters,
    options,
    req.query.dept
  );
  res.send(result);
});

const getUserPerformance = catchAsync(async (req, res) => {
  const type = req.query.type;
  const result = await performanceService.queryUserPerformances(req.user, type);
  res.send(result);
});

const updatePerformance = catchAsync(async (req, res) => {
  const performance = await performanceService.updatePerformanceById(
    req.params.performanceId,
    req.body
  );
  res.send(performance);
});

module.exports = {
  createPerformance,
  getPerformances,
  updatePerformance,
  getUserPerformance,
};
