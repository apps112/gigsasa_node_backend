const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { answersService } = require("../services");

const createAnswers = catchAsync(async (req, res) => {
  const timesheet = await answersService.createAnswers(req.body);
  res.status(httpStatus.CREATED).send(timesheet);
});

const getAnswers = catchAsync(async (req, res) => {
  const result = await answersService.queryAnswers(req.query.question);
  res.send(result);
});

const getAllocationAnswers = catchAsync(async (req, res) => {
  const result = await answersService.queryAllocationAnswers(
    req.query.allocationId
  );
  res.send(result);
});

module.exports = {
  createAnswers,
  getAnswers,
  getAllocationAnswers,
};
