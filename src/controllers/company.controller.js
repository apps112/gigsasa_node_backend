const httpStatus = require("http-status");
const validator = require("validator");

const catchAsync = require("../utils/catchAsync");
const {
  companyService,
  userService,
  emailService,
  tokenService,
} = require("../services");

const createCompany = catchAsync(async (req, res) => {
  // create a company
  const company = await companyService.createCompany(req.body);
  // update user object
  await userService.updateUserById(req.user._id, { company_id: company._id });

  if (company.invite_email && validator.isEmail(company.invite_email)) {
    // check if there is already a user or not
    const isExistingUser = await userService.getUserByEmail(
      company.invite_email
    );

    console.log("isExistingUser", isExistingUser);

    if (!isExistingUser) {
      // send mail to co-partner or admin
      const joinCompanyToken = await tokenService.generateJoinCompanyToken(
        company._id,
        req.user,
        company.invite_email
      );
      await emailService.sendCompanyInvitationEmail(
        company.invite_email,
        joinCompanyToken,
        company.name,
        req.user.name
      );
    }
  }

  res.status(httpStatus.CREATED).send(company);
});

const getCompany = catchAsync(async (req, res) => {
  const result = await companyService.getCompany(req.query.companyId);
  res.send(result);
});

const updateCompany = catchAsync(async (req, res) => {
  const company = await companyService.updateCompanyById(
    req.params.companyId,
    req.body
  );
  res.send(company);
});

module.exports = {
  createCompany,
  getCompany,
  updateCompany,
};
