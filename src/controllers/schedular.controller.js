const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const {
  schedularService,
  userService,
  genericService,
} = require("../services");
const pick = require("../utils/pick");
const { isRootUser } = require("../utils/userType");

const createSchedular = catchAsync(async (req, res) => {
  const schedule = await schedularService.createSchedular(req.body);
  let populated = await schedularService.findOnePopulate(schedule.id);
  res.status(httpStatus.CREATED).send(populated);
});

const updateSchedular = catchAsync(async (req, res) => {
  const schedule = await schedularService.updateSchedularById(
    req.params.schedularId,
    req.body
  );
  let populated = await schedularService.findOnePopulate(schedule.id);
  res.status(httpStatus.CREATED).send(populated);
});

const deleteSchedular = catchAsync(async (req, res) => {
  let { id } = req.body;
  const result = await schedularService.deleteById(id);
  res.status(httpStatus.CREATED).send(result);
});

const getSchedular = catchAsync(async (req, res) => {
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id)
    : await genericService.getUserDescendants(user);
  const results = await schedularService.querySchedular(filters, options);
  res.send(results);
});

const userSummary = catchAsync(async (req, res) => {
  const results = await schedularService.userSummary(req.user);
  res.send(results);
});

module.exports = {
  createSchedular,
  getSchedular,
  updateSchedular,
  deleteSchedular,
  userSummary
};
