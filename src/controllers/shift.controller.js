const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const {
  shiftService,
  activityService,
  timesheetService,
  answersService,
  userService,
  genericService,
} = require("../services");
const pick = require("../utils/pick");
const { isRootUser } = require("../utils/userType");
const { parseInOuts } = require("../utils/dailyActivityUtil");

const createShift = catchAsync(async (req, res) => {
  // create shift
  const shift = await shiftService.createShift(req.body);
  // create allocations
  const allocations = await shiftService.createAllocations(req.body, shift);
  res.status(httpStatus.CREATED).send(allocations);
});

const getAllocations = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);
  const result = await shiftService.queryAllocations(req.query, filters);
  res.send(result);
});

const getUserAllocations = catchAsync(async (req, res) => {
  const user = req.user;
  const result = await shiftService.queryUserAllocations(req.query, user);
  res.send(result);
});

const currentAllocation = catchAsync(async (req, res) => {
  const result = await shiftService.queryCurrentAllocation(req.user);
  res.send(result);
});

const getUpcoming = catchAsync(async (req, res) => {
  let page = req.query.page;
  const result = await shiftService.getUpcoming(req.user, page);
  res.send(result);
});

const updateShift = catchAsync(async (req, res) => {
  const shift = await shiftService.updateShiftById(
    req.params.shiftId,
    req.body
  );
  res.send(shift);
});

const clockIn = catchAsync(async (req, res) => {
  // clock user in
  const allocation = await shiftService.clockInAllocation(req.body, req.user);
  // add activities
  await activityService.createActivity(
    allocation,
    "clockin",
    req.body.clockInByAdmin
  );
  if (req.body.answers && req.body.answers.length > 0) {
    let data = req.body.answers.map((obj) => ({
      ...obj,
      allocation: allocation.id,
    }));
    await answersService.createAnswers(data, req.user);
  }
  res.send(allocation);
});

const breakIn = catchAsync(async (req, res) => {
  const allocation = await shiftService.breakInAllocation(req.body);
  await activityService.createActivity(
    allocation,
    "breakin",
    req.body.inByAdmin
  );
  res.send(allocation);
});

const breakOut = catchAsync(async (req, res) => {
  const allocation = await shiftService.breakOutAllocation(req.body);
  await activityService.createActivity(
    allocation,
    "breakout",
    req.body.outByAdmin
  );
  res.send(allocation);
});

const clockOut = catchAsync(async (req, res) => {
  const allocation = await shiftService.clockOutAllocation(req.body);
  await activityService.createActivity(
    allocation,
    "clockout",
    req.body.clockOutByAdmin
  );
  if (req.body.answers && req.body.answers.length > 0) {
    let data = req.body.answers.map((obj) => ({
      ...obj,
      allocation: allocation.id,
    }));
    await answersService.createAnswers(data, req.user);
  }
  res.send(allocation);
  // create timesheet service
  await timesheetService.createTimesheet(allocation);
});

const getInOuts = catchAsync(async (req, res) => {
  const user = req.user;
  const deptUsers = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const clockInOptions = { ...options, status: "going", query: "clockin" };
  const clockIns = shiftService.queryInOuts(deptUsers, clockInOptions);
  const clockOutOptions = {
    ...options,
    status: "completed",
    query: "clockout",
  };
  const clockOuts = shiftService.queryInOuts(deptUsers, clockOutOptions);
  const breakOptions = { ...options, status: "break", query: "clockin" };
  const breakIns = shiftService.queryInOuts(deptUsers, breakOptions);
  const [clockInResults, clockOutResults, breakInResults] = await Promise.all([
    clockIns,
    clockOuts,
    breakIns,
  ]);
  // parse results
  const parsedResults = parseInOuts({
    clockInResults,
    clockOutResults,
    breakInResults,
  });

  res.send(parsedResults);
});

const getTodayActive = async (req, res) => {
  const user = req.user;
  const deptUsers = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);
  const filterUsers = deptUsers.filter((id) => id !== user.id);
  const todayActive = await shiftService.queryTodayActives(filterUsers);
  res.send(todayActive);
};

const getTodayOnTimes = async (req, res) => {
  const user = req.user;

  const deptUsers = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const filterUsers = deptUsers.filter((id) => id !== user.id);
  const todayOnTime = await shiftService.queryOnTime(filterUsers);
  res.send(todayOnTime);
};

const getWeeklyActivity = async (req, res) => {
  const user = req.user;

  const deptUsers = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const weeklyActivities = await shiftService.queryWeeklyActivity(deptUsers);
  res.send(weeklyActivities);
};

const deleteAllocation = async (req, res) => {
  await shiftService.deleteAllocation(req.params.allocationId);
  res.status(httpStatus.NO_CONTENT).send();
};

module.exports = {
  createShift,
  getAllocations,
  updateShift,
  clockIn,
  breakIn,
  breakOut,
  clockOut,
  getInOuts,
  getTodayActive,
  getTodayOnTimes,
  getWeeklyActivity,
  deleteAllocation,
  currentAllocation,
  getUpcoming,
  getUserAllocations,
};
