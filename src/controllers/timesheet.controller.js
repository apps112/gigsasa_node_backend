const httpStatus = require("http-status");
const mongoose = require("mongoose");
const catchAsync = require("../utils/catchAsync");
const {
  timesheetService,
  userService,
  genericService,
  shiftService,
  leavesService,
} = require("../services");
const { isRootUser } = require("../utils/userType");
const { getUserIds, parseLeaveTimeSheet } = require("../utils/parseSheet");
const moment = require("moment");

const createTimesheet = catchAsync(async (req, res) => {
  let promises = [];
  req.body.map((item) => {
    promises.push(timesheetService.createTimesheet(item, req.user.id));
    return;
  });
  let results = await Promise.all([...promises]);
  res.status(httpStatus.CREATED).send(results[0]);
});

const requestTimesheet = catchAsync(async (req, res) => {
  let promises = [];
  req.body.map((item) => {
    promises.push(timesheetService.requestTimesheet(item));
    return;
  });
  let result = await Promise.all([...promises]);
  res.status(httpStatus.CREATED).send(result);
});

const updateRequest = catchAsync(async (req, res) => {
  const timesheet = await timesheetService.updateRequest(req.body, req.query);
  res.send(timesheet);
});

const getRequests = catchAsync(async (req, res) => {
  const timesheets = await timesheetService.getRequests(
    req.query.page,
    req.user
  );
  res.send(timesheets);
});

const getTimesheets = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const timesheetRes = await timesheetService.queryTimesheets(
    filters,
    req.query
  );

  console.log("filters", filters);
  // const  userIds= getUserIds(timesheetRes);
  const leaveRes = await leavesService.getAllLeaves(req.query, filters);

  console.log("leaveRes", leaveRes);
  const parsedTimeSheets = parseLeaveTimeSheet(
    timesheetRes,
    leaveRes,
    req.query
  );

  res.send(parsedTimeSheets);
});

const deleteTimesheet = catchAsync(async (req, res) => {
  let { timesheets, allocation } = req.body;
  let one = timesheetService.deleteTimesheet(timesheets);
  let two = shiftService.deleteAllocationByID(allocation);
  let results = await Promise.all([one, two]);
  res.send(results);
});

const getDailyTimesheets = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);
  const result = await timesheetService.queryDailyTimesheets(
    filters,
    req.query
  );
  const start = moment().startOf("day").toDate();
  const end = moment(start).endOf("day").toDate();

  const leaveRes = await leavesService.getAllLeaves({ start, end }, filters);

  const parsedTimeSheets = parseLeaveTimeSheet(result, leaveRes, {
    start,
    end,
  });

  res.send(parsedTimeSheets);
});

const getPending = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);
  const result = await timesheetService.getPendingTimesheets(
    filters,
    req.query
  );
  res.send(result);
});

const updateStatus = catchAsync(async (req, res) => {
  const timesheet = await timesheetService.updateStatus(req.body);
  res.send(timesheet);
});

const getUserTimesheets = catchAsync(async (req, res) => {
  const result = await timesheetService.queryUserTimesheets(
    req.user,
    req.query
  );
  res.send(result);
});

const getSpecificTimesheets = catchAsync(async (req, res) => {
  const timeSheetDetails = timesheetService.querySpecificTimesheets(
    req.body.id,
    req.query
  );

  const userDetails = userService.getUserById(req.body.id);
  const leavesRes = leavesService.getSpecificLeaves(req.query, req.body.id);
  const [timeSheets, user, leaves] = await Promise.all([
    timeSheetDetails,
    userDetails,
    leavesRes,
  ]);

  const parsedTimeSheets = parseLeaveTimeSheet(timeSheets, leaves, req.query);

  res.send({ timesheets: parsedTimeSheets, user });
});

const getTotalTimes = catchAsync(async (req, res) => {
  const { queryType } = req.query;
  if (["daily", "custom"].includes(queryType)) {
    const user = req.user;
    let filters = isRootUser(user)
      ? await userService.getComUsersIds(user.company_id, req.query.dept)
      : await genericService.getUserDescendants(user, req.query.dept);
    filters = filters.map((id) => mongoose.Types.ObjectId(id));
    const totalTimes = await timesheetService.aggregateTimesDaily(
      filters,
      req.query
    );
    res.send(totalTimes);
    return;
  }
  const totalTimes = await timesheetService.aggregateTimes(
    req.body.id,
    req.query
  );
  res.send(totalTimes);
});

const getTimeSheetDataForExport = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const result = await timesheetService.getExportData(filters, req.query);
  res.send(result);
});

const editTime = catchAsync(async (req, res) => {
  let { allocation, clockin, clockout, timesheets, activity } = req.body;
  // update allocation times
  let result = await shiftService.updateAllocationTime(
    allocation,
    clockin,
    clockout,
    activity
  );
  // update timesheet Times
  let updatedTimesheet = await timesheetService.updateTimesheet(
    result,
    timesheets,
    req.user.id
  );
  res.send(updatedTimesheet);
});

module.exports = {
  createTimesheet,
  requestTimesheet,
  updateRequest,
  getTimesheets,
  getRequests,
  getUserTimesheets,
  getDailyTimesheets,
  getSpecificTimesheets,
  getTotalTimes,
  getTimeSheetDataForExport,
  editTime,
  getPending,
  updateStatus,
  deleteTimesheet,
};
