const httpStatus = require("http-status");
const pick = require("../utils/pick");
const ApiError = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");
const {
  userService,
  emailService,
  tokenService,
  organizationService,
  genericService,
} = require("../services");
const { tokenTypes } = require("../config/tokens");
const config = require("../config/config");

const createUser = catchAsync(async (req, res) => {
  const randomPS = Math.random().toString(36).slice(-8);
  const { organization, jobType } = req.body;
  const userObject = {
    ...req.body,
    password: randomPS,
    isInitialPassword: true,
  };
  const one = userService.createUser(userObject);
  const two = organizationService.addJobTypeToOrganization(
    organization,
    jobType
  );
  let results = await Promise.all([one, two]);
  let user = results[0];
  const verifyEmailToken = await tokenService.generateVerifyEmailToken(user);
  // add user to organization
  const updateOrg = organizationService.addMemberToOrganization(
    organization,
    user
  );
  // add user to the creator descendants
  const updateUser = userService.addUserToDescendants(user, req.user.id);
  // send mail to newly added user
  const sendMail = emailService.sendNewUserMail(userObject, verifyEmailToken);
  await Promise.all([updateUser, sendMail, updateOrg]);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  if (req.query.getAll) {
    const result = await userService.getAllUsers(req.user, req.query.dept);
    res.send(result);
    return;
  }
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await userService.queryUsers(
    options,
    req.user,
    req.query.dept
  );
  res.send(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.send(user);
});

const updateProfile = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.user.id, req.body);
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUser(req.params.userId);

  res.status(httpStatus.NO_CONTENT).send();
});

const subscriptionCreated = catchAsync(async (req, res) => {
  let subscription = req.body.content.subscription;
  let user = await userService.getUserByEmail(req.body.content.customer.email);
  if (user) {
    await userService.updateUserById(user.id, {
      subscription_plan: {
        plan: subscription.plan_id,
        start: new Date(subscription.trial_start),
        end: new Date(subscription.trial_end),
      },
    });
  }
  res.status(httpStatus.NO_CONTENT).send();
});

const subscriptionCancelled = catchAsync(async (req, res) => {
  let user = await userService.getUserByEmail(req.body.content.customer.email);
  if (user) {
    await userService.updateUserById(user.id, {
      subscription_plan: null,
    });
  }
  res.status(httpStatus.NO_CONTENT).send();
});

const createMultipleUsers = catchAsync(async (req, res) => {
  const userObjects = req.body.map((user) => {
    const randomPS = Math.random().toString(36).slice(-8);
    return {
      ...user,
      password: randomPS,
      isInitialPassword: true,
    };
  });

  const users = await userService.createMultiple(userObjects);
  // add user to the creator descendants

  const userIds = users.map((user) => user.id);
  await userService.addMultipleToDescendants(userIds, req.user.id);

  // send email to all users
  await Promise.all(
    users.map(async (user) => {
      const verifyEmailToken = await tokenService.generateVerifyEmailToken(
        user
      );
      const userObj = userObjects.filter(
        (item) => item.email === user.email
      )[0];
      return emailService.sendNewUserMail(userObj, verifyEmailToken);
    })
  );
  res.status(httpStatus.CREATED).send(users);
});

const joinUserRequest = catchAsync(async (req, res) => {
  const requestee = await userService.getUserByEmail(req.body.email);
  console.log("user requestor", requestee.role);
  if (!requestee || !["admin", "root", "supervisor"].includes(requestee.role)) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }

  const joinToken = await tokenService.generateJoinComRequestToken(
    req.user,
    requestee
  );
  console.log("req.body", joinToken);
  await emailService.sendJoinComReqMail({
    user: req.user,
    token: joinToken,
    email: req.body.email,
  });
  res.status(httpStatus.NO_CONTENT).send();
});

const acceptJoinReq = catchAsync(async (req, res) => {
  try {
    const verifyJoinToken = await tokenService.verifyToken(
      req.query.token,
      tokenTypes.JOIN_COMPANY_REQ
    );

    await userService.acceptJoin(verifyJoinToken);
    res.redirect(`${config.CLIENT_URL}app/members`);
  } catch (err) {
    res.json("error occur while accept join request");
  }
});

const updateTourStatus = catchAsync(async (req, res) => {
  const { status, userId } = req.body;

  await userService.updateUserById(userId, { tourStatus: status });
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  createMultipleUsers,
  updateProfile,
  subscriptionCreated,
  subscriptionCancelled,
  joinUserRequest,
  acceptJoinReq,
  updateTourStatus,
};
