const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const pick = require("../utils/pick");
const { timerService } = require("../services");

const createTimer = catchAsync(async (req, res) => {
  const timer = await timerService.createTimer({
    ...req.body,
  });

  res.status(httpStatus.CREATED).send(timer);
});

const saveBreak = catchAsync(async (req, res) => {
  let { allocation } = req.body;
  let data = pick(req.body, ["breakTime", "counter"]);
  const timer = await timerService.saveBreak(allocation, data);
  res.send(timer);
});

const saveBreakout = catchAsync(async (req, res) => {
  let { allocation, breaks } = req.body;
  const timer = await timerService.saveBreakout(allocation, breaks);
  res.send(timer);
});

module.exports = {
  createTimer,
  saveBreak,
  saveBreakout,
};
