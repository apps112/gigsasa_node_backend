const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { complainService, emailService } = require("../services");

const createComplain = catchAsync(async (req, res) => {
  // create a complain
  const complain = await complainService.createComplain({
    ...req.body,
    user: req.user.id,
  });

  // send complain mail to app admin
  await emailService.sendComplainMail(complain);

  res.status(httpStatus.CREATED).send(complain);
});

module.exports = {
  createComplain,
};
