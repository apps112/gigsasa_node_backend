const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { leavesService, userService, genericService } = require("../services");
const { isRootUser } = require("../utils/userType");

const createLeaves = catchAsync(async (req, res) => {
  const leave = await leavesService.createLeaves(req.body);
  res.status(httpStatus.CREATED).send(leave);
});

const createType = catchAsync(async (req, res) => {
  const type = await leavesService.createType(req.body);
  res.status(httpStatus.CREATED).send(type);
});

const updateType = catchAsync(async (req, res) => {
  let id = req.query.typeId;
  const type = await leavesService.updateType(id, req.body);
  res.status(httpStatus.CREATED).send(type);
});

const deleteType = catchAsync(async (req, res) => {
  let id = req.query.typeId;
  const result = await leavesService.deleteType(id);
  res.send(result);
});

const getLeaves = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const result = await leavesService.queryLeaves(req.query.page, filters);
  res.send(result);
});

const getLeaveTotals = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const result = await leavesService.queryLeavesTotals(filters);
  res.send(result);
});

const getTypes = catchAsync(async (req, res) => {
  let { company } = req.query;
  const result = await leavesService.getTypes(company);
  res.send(result);
});

const getUserLeaves = catchAsync(async (req, res) => {
  const result = await leavesService.queryUserLeaves(req.query.page, req.user);
  res.send(result);
});

const updateStatus = catchAsync(async (req, res) => {
  const result = await leavesService.updateStatus(req.query.id, req.body);
  res.send(result);
});

module.exports = {
  createLeaves,
  getLeaves,
  getUserLeaves,
  updateStatus,
  getTypes,
  createType,
  updateType,
  deleteType,
  getLeaveTotals,
};
