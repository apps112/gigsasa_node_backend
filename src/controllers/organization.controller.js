const httpStatus = require("http-status");
var mongoose = require("mongoose");
const pick = require("../utils/pick");
const catchAsync = require("../utils/catchAsync");
const {
  organizationService,
  userService,
  genericService,
} = require("../services");

const { isRootUser } = require("../utils/userType");

const createOrganization = catchAsync(async (req, res) => {
  let { parent } = req.body;
  const organization = await organizationService.createOrganization(req.body);
  // update parent organization
  let one = organizationService.addOrgToDescendants(parent, organization);
  // update user
  let two = userService.addOrgToDescendants(organization, req.user.id);
  await Promise.all([one, two]);
  res.status(httpStatus.CREATED).send(organization);
});

const getOrganizations = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await organizationService.getComOrganizationIds(user.company_id)
    : await genericService.getDeptDescendants(user);

  if (req.query.getAll) {
    const result = await organizationService.getAllOrganizations(filters);
    return res.send(result);
  }
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await organizationService.queryOrganizations(filters, options);
  res.send(result);
});

const updateOrganization = catchAsync(async (req, res) => {
  let id = req.params.organizationId;

  if (req.body.parent && mongoose.isValidObjectId(req.body.parent)) {
    // update and modify from parent descendants
    await organizationService.deleteDescendantsFromParent(id);
    await organizationService.addOrgToDescendants(req.body.parent, { id });
  }

  const organization = await organizationService.updateOrganizationById(
    id,
    req.body
  );

  res.send(organization);
});

module.exports = {
  createOrganization,
  getOrganizations,
  updateOrganization,
};
