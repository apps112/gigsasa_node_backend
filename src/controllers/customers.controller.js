const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const pick = require("../utils/pick");
const {
  customersService,
  organizationService,
  genericService,
} = require("../services");
const { isRootUser } = require("../utils/userType");

const createCustomer = catchAsync(async (req, res) => {
  const customer = await customersService.createCustomer(req.body);
  res.status(httpStatus.CREATED).send(customer);
});

const getCustomers = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? await organizationService.getComOrganizationIds(user.company_id)
    : await genericService.getDeptDescendants(user);
  if (req.query.getAll) {
    const result = await customersService.getAllCustomers(filters);
    res.send(result);
    return;
  }
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await customersService.queryCustomers(filters, options);
  res.send(result);
});

const getUserCustomers = catchAsync(async (req, res) => {
  const result = await customersService.getUserCustomers(req.user.organization);
  res.send(result);
});

const getSpecificCustomers = catchAsync(async (req, res) => {
  let { organization } = req.query;
  const result = await customersService.getUserCustomers(organization);
  res.send(result);
});

const updateCustomer = catchAsync(async (req, res) => {
  const id = req.params.customerId;

  const customer = await customersService.updateCustomer(id, req.body);

  res.send(customer);
});

module.exports = {
  createCustomer,
  getCustomers,
  updateCustomer,
  getUserCustomers,
  getSpecificCustomers,
};
