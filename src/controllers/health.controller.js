const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { healthService, userService, genericService } = require("../services");
const pick = require("../utils/pick");
const { isRootUser } = require("../utils/userType");

const createHealthEmergency = catchAsync(async (req, res) => {
  // create a complain
  const health = await healthService.createHealthEmergency({
    ...req.body,
    user: req.user.id,
  });

  // find supervisor phone number

  let result = await userService.getSupervisorAdmin(req.user);
  res.status(httpStatus.CREATED).send(result);
});

const getHealthEmergencies = catchAsync(async (req, res) => {
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const user = req.user;

  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  // create a complain
  const results = await healthService.queryHealthEmergencies(filters, options);

  res.send(results);
});

module.exports = {
  createHealthEmergency,
  getHealthEmergencies,
};
