const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const ApiError = require("../utils/ApiError");
const {
  authService,
  userService,
  tokenService,
  emailService,
} = require("../services");
const config = require("../config/config");
const Token = require("../models/token.model");
const { tokenTypes } = require("../config/tokens");

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);

  if (!user.isEmailVerified) {
    // send email for verification
    const verifyEmailToken = await tokenService.generateVerifyEmailToken(user);
    await emailService.sendVerificationEmail(
      user.email,
      verifyEmailToken,
      user.name
    );
  }
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const login = catchAsync(async (req, res) => {
  const { email, password, isSocialAuth } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(
    email,
    password,
    isSocialAuth
  );
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const getCurrentUser = catchAsync(async (req, res) => {
  let user = req.user;
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const loginWithPhone = catchAsync(async (req, res) => {
  const { phone, password } = req.body;
  const user = await authService.loginUserWithPhoneAndPassword(phone, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(
    req.body.email
  );
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const forgotPasswordMobile = catchAsync(async (req, res) => {
  let { email, code } = req.body;
  let user = await userService.getUserByEmail(email);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User with this email not exist");
  }
  await emailService.sendResetPasswordEmailMobile(user, code);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPasswordMobile = catchAsync(async (req, res) => {
  let { email, password } = req.body;
  let user = await userService.getUserByEmail(email);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User with this email not exist");
  }
  await authService.resetPasswordMobile(user, password);
  res.status(httpStatus.NO_CONTENT).send();
});

const updatePassword = catchAsync(async (req, res) => {
  let user = await authService.updatePassword(req.user, req.body.password);
  res.send(user);
});

const changePassword = catchAsync(async (req, res) => {
  let user = await authService.changePassword(req.user, req.body);
  res.send(user);
});

const setImage = catchAsync(async (req, res) => {
  let user = await authService.setProfile(req.user, req.body.profilePic);
  res.send(user);
});

const sendVerificationEmail = catchAsync(async (req, res) => {
  const verifyEmailToken = await tokenService.generateVerifyEmailToken(
    req.user
  );
  await emailService.sendVerificationEmail(
    req.user.email,
    verifyEmailToken,
    req.user.name
  );
  res.status(httpStatus.NO_CONTENT).send();
});

const verifyEmail = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.query.token);
  res.redirect(config.CLIENT_URL);
});

const verifyEmailUser = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.query.token);
  res.redirect("http://gigsasa.com/download");
});

const registerAdmin = catchAsync(async (req, res) => {
  const { token, name, password, isEmailVerified } = req.body;

  // get token from token id
  const tokenDoc = await authService.verifyInviteCompany(token);

  const userObject = {
    name,
    password,
    isEmailVerified,
    email: tokenDoc.requesteeEmail,
    role: "admin",
    company_id: tokenDoc.companyId,
    active: true,
    isInitialPassword: false,
  };

  console.log("tokenObj", userObject);

  const user = await userService.createUser(userObject);
  const tokens = await tokenService.generateAuthTokens(user);

  const deleteToken = Token.deleteMany({
    user: tokenDoc.user,
    type: tokenTypes.JOIN_COMPANY,
  });

  const addUserToDesc = userService.addUserToDescendants(user, tokenDoc.user);
  await Promise.all([deleteToken, addUserToDesc]);

  res.status(httpStatus.CREATED).send({ user, tokens });
});

module.exports = {
  register,
  login,
  getCurrentUser,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  sendVerificationEmail,
  verifyEmail,
  loginWithPhone,
  updatePassword,
  setImage,
  changePassword,
  forgotPasswordMobile,
  resetPasswordMobile,
  verifyEmailUser,
  registerAdmin,
};
