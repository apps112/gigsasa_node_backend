const httpStatus = require("http-status");
const pick = require("../utils/pick");
const catchAsync = require("../utils/catchAsync");
const { locationService } = require("../services");
const { isRootUser } = require("../utils/userType");

const createLocation = catchAsync(async (req, res) => {
  const location = await locationService.createLocation(req.body);
  res.status(httpStatus.CREATED).send(location);
});

const getLocations = catchAsync(async (req, res) => {
  const user = req.user;
  const filters = isRootUser(user)
    ? { company: req.query.company }
    : { user: user.id, company: req.query.company };
  if (req.query.getAll) {
    const result = await locationService.getAllLocations(filters);
    res.send(result);
    return;
  }
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await locationService.queryLocations(filters, options);
  res.send(result);
});

const updateLocation = catchAsync(async (req, res) => {
  const location = await locationService.updateLocationById(
    req.params.locationId,
    req.body
  );
  res.send(location);
});

const deleteLocation = catchAsync(async (req, res) => {
  await locationService.deleteLocation(req.params.locationId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createLocation,
  getLocations,
  updateLocation,
  deleteLocation,
};
