const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const pick = require("../utils/pick");
const {
  analyticService,
  userService,
  timesheetService,
  genericService,
} = require("../services");
const dataset = require("../utils/dataset.json");
const { isRootUser } = require("../utils/userType");
const { parsePowerBI } = require("../utils/parseSheet");

const getSampleReports = catchAsync(async (req, res) => {
  const sampleReports = await analyticService.getDefaultReports();

  // generate Tokens

  const reportIds = sampleReports.value.map((item) => {
    return {
      id: item.id,
    };
  });

  const defaultDatasets = await analyticService.getDefaultDatasets();

  const datasetIds = defaultDatasets.value.map((item) => {
    return {
      id: item.id,
    };
  });

  const tokenData = {
    datasets: datasetIds,
    reports: reportIds,
  };

  const token = await analyticService.getToken(tokenData);

  res.send({
    reports: sampleReports.value,
    token: token,
  });
});

const addDashboard = catchAsync(async (req, res) => {
  const { sampleReportId } = req.body;
  const user = req.user;

  const workspaceName = {
    name: user.id,
  };

  // create workspace
  const workspace = await analyticService.createWorkSpace(workspaceName);

  // create dataset
  const newDataset = await analyticService.createDataset(workspace.id, dataset);

  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id)
    : await genericService.getUserDescendants(user);

  const result = await timesheetService.getExportData(filters, req.query);

  const parsedPBI = parsePowerBI(result);

  // create dataset

  await analyticService.createRows({
    groupId: workspace.id,
    data: { rows: parsedPBI },
    datasetId: newDataset.id,
    tableName: "Sheet1",
  });

  // clone report

  const targetReport = await analyticService.cloneReport({
    reportId: sampleReportId,
    data: {
      name: req.user.name,
      targetWorkspaceId: workspace.id,
      targetModelId: newDataset.id,
    },
  });

  // update user object

  const userObj = {
    workspaceId: workspace.id,
    datasetId: newDataset.id,
    reportId: targetReport.id,
    embedUrl: targetReport.embedUrl,
  };

  // getToken
  const tokenData = {
    datasets: [
      {
        id: newDataset.id,
      },
    ],
    reports: [
      {
        id: targetReport.id,
      },
    ],
  };

  const updateUser = userService.updateUserById(req.user.id, {
    powerBi: userObj,
  });

  const tokenReq = await analyticService.getToken(tokenData);

  const [userUpdated, token] = await Promise.all([updateUser, tokenReq]);

  res.send({
    userPowerBi: userObj,
    token,
  });
});

const updatePowerBiData = catchAsync(async (req, res) => {
  try {
    const user = req.user;
    const { powerBi } = user;

    if (!powerBi) {
      return res.send("powerbi is not initialized");
    }

    const filters = isRootUser(user)
      ? await userService.getComUsersIds(user.company_id)
      : await genericService.getUserDescendants(user);

    const result = await timesheetService.getExportData(filters, req.query);

    const parsedPBI = parsePowerBI(result);

    console.log("parsedPBI", parsedPBI);

    // delete existing rows

    const deleteRows = await analyticService.deleteRows({
      datasetId: powerBi.datasetId,
      groupId: powerBi.workspaceId,
      tableName: "Sheet1",
    });

    const addRows = await analyticService.createRows({
      groupId: powerBi.workspaceId,
      data: { rows: parsedPBI },
      datasetId: powerBi.datasetId,
      tableName: "Sheet1",
    });

    // getToken
    const tokenData = {
      datasets: [
        {
          id: powerBi.datasetId,
        },
      ],
      reports: [
        {
          id: powerBi.reportId,
        },
      ],
    };

    const token = await analyticService.getToken(tokenData);

    res.send(token);
  } catch (err) {
    console.log("err while ", err);
  }
});

module.exports = {
  getSampleReports,
  addDashboard,
  updatePowerBiData,
};
