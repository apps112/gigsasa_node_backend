const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { activityService, userService, genericService } = require("../services");
const pick = require("../utils/pick");
const { isRootUser } = require("../utils/userType");

const getActivities = catchAsync(async (req, res) => {
  const options = pick(req.query, ["sortBy", "limit", "page"]);

  const user = req.user;

  const filters = isRootUser(user)
    ? await userService.getComUsersIds(user.company_id, req.query.dept)
    : await genericService.getUserDescendants(user, req.query.dept);

  const result = await activityService.queryActivities(filters, options);
  res.send(result);
});

module.exports = {
  getActivities,
};
