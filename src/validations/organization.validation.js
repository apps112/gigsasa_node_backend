const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createOrganization = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    numberOfMembers: Joi.number().integer(),
    companyId: Joi.string().required(),
    parent: Joi.custom(objectId),
  }),
};

const getOrganizations = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    getAll: Joi.boolean(),
  }),
};

const updateOrganizations = {
  params: Joi.object().keys({
    organizationId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      jobTypes: Joi.array().items(Joi.string()),
      numberOfMembers: Joi.number().integer(),
      parent: Joi.custom(objectId),
    })
    .min(1),
};

module.exports = {
  createOrganization,
  getOrganizations,
  updateOrganizations,
};
