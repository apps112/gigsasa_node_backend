const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createLocation = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    address: Joi.string().required(),
    radius: Joi.number(),
    coordinates: Joi.object().keys({
      latitude: Joi.number().required(),
      longitude: Joi.number().required(),
    }),
    company: Joi.required().custom(objectId),
    user: Joi.required().custom(objectId),
  }),
};

const getLocations = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    company: Joi.string().required(),
    getAll: Joi.boolean(),
  }),
};

const updateLocation = {
  params: Joi.object().keys({
    locationId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      address: Joi.string(),
      radius: Joi.number(),
      coordinates: Joi.object().keys({
        latitude: Joi.number(),
        longitude: Joi.number(),
      }),
    })
    .min(1),
};

const deleteLocation = {
  params: Joi.object().keys({
    locationId: Joi.required().custom(objectId),
  }),
};

module.exports = {
  createLocation,
  getLocations,
  updateLocation,
  deleteLocation,
};
