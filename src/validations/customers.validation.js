const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createCustomer = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    type: Joi.string().required(),
    departments: Joi.array().items(Joi.required().custom(objectId)),
    createdBy: Joi.required().custom(objectId),
    company: Joi.required().custom(objectId),
  }),
};

const getCustomers = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    getAll: Joi.boolean(),
  }),
};

const getSpecificCustomers = {
  query: Joi.object().keys({
    organization: Joi.custom(objectId),
  }),
};

const updateCustomer = {
  params: Joi.object().keys({
    customerId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      type: Joi.string(),
      departments: Joi.array().items(Joi.custom(objectId)),
    })
    .min(1),
};

module.exports = {
  createCustomer,
  getCustomers,
  updateCustomer,
  getSpecificCustomers,
};
