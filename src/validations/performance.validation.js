const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createPerformance = {
  body: Joi.object().keys({
    question: Joi.string().required(),
    type: Joi.string().valid("number", "options", "text"),
    questionType: Joi.string().valid("in", "out"),
    options: Joi.array().items(Joi.string()),
    organization: Joi.required().custom(objectId),
    jobType: Joi.string().required(),
  }),
};

const getPerformance = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    dept: Joi.custom(objectId),
  }),
};

const getUserPerformance = {
  query: Joi.object().keys({
    type: Joi.string().required(),
  }),
};

const updatePerformance = {
  params: Joi.object().keys({
    performanceId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      question: Joi.string(),
      type: Joi.string().valid("number", "options", "text"),
      questionType: Joi.string().valid("in", "out"),
      options: Joi.array().items(Joi.string()),
      organization: Joi.custom(objectId),
      jobType: Joi.string(),
      active: Joi.boolean(),
    })
    .min(1),
};

module.exports = {
  createPerformance,
  getPerformance,
  updatePerformance,
  getUserPerformance,
};
