const Joi = require("joi");

const createReport = {
  body: Joi.object().keys({
    sampleReportId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
  }),
};

const updatePBI = {
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
  }),
};
module.exports = {
  createReport,
  updatePBI,
};
