const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createLeaves = {
  body: Joi.object().keys({
    type: Joi.string().required(),
    user: Joi.required().custom(objectId),
    start: Joi.date().required(),
    end: Joi.date().required(),
    description: Joi.string().required(),
    leavePeriod: Joi.string(),
    paid: Joi.bool(),
    status: Joi.string(),
  }),
};

const getLeaves = {
  query: Joi.object().keys({
    page: Joi.number().required(),
    dept: Joi.custom(objectId),
  }),
};

const getLeaveTotals = {
  query: Joi.object().keys({
    dept: Joi.custom(objectId),
  }),
};

const createType = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    company: Joi.required().custom(objectId),
  }),
};

const getTypes = {
  query: Joi.object().keys({
    company: Joi.required().custom(objectId),
  }),
};

const updateType = {
  query: Joi.object().keys({
    typeId: Joi.required().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string(),
  }),
};

const deleteType = {
  query: Joi.object().keys({
    typeId: Joi.required().custom(objectId),
  }),
};

const getUserLeaves = {
  query: Joi.object().keys({
    page: Joi.number().required(),
  }),
};

const updateStatus = {
  query: Joi.object().keys({
    id: Joi.required().custom(objectId),
  }),
  body: Joi.object().keys({
    status: Joi.string().valid("approved", "rejected"),
  }),
};

module.exports = {
  createLeaves,
  getLeaves,
  getTypes,
  getUserLeaves,
  updateStatus,
  createType,
  updateType,
  deleteType,
  getLeaveTotals,
};
