const Joi = require("joi");
const { objectId } = require("./custom.validation");

const getActivities = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    getAll: Joi.boolean(),
    dept: Joi.custom(objectId),
  }),
};

module.exports = {
  getActivities,
};
