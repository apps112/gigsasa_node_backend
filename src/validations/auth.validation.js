const Joi = require("joi");
const { password } = require("./custom.validation");

const register = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    phone: Joi.string(),
    isSocialAuth: Joi.boolean(),
    isEmailVerified: Joi.boolean(),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
    isSocialAuth: Joi.boolean(),
  }),
};

const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
  }),
};

const forgotPasswordMobile = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
    code: Joi.number().required(),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const resetPasswordMobile = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
    email: Joi.string().required(),
  }),
};

const updatePassword = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const changePassword = {
  body: Joi.object().keys({
    oldPassword: Joi.string().required().custom(password),
    password: Joi.string().required().custom(password),
  }),
};

const setImage = {
  body: Joi.object().keys({
    profilePic: Joi.string().required(),
  }),
};

const verifyEmail = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
};

const registerAdmin = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    phone: Joi.string(),
    token: Joi.string().required(),
    isEmailVerified: Joi.boolean(),
  }),
};

module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  forgotPasswordMobile,
  resetPasswordMobile,
  resetPassword,
  verifyEmail,
  updatePassword,
  setImage,
  changePassword,
  registerAdmin,
};
