const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createAnswers = {
  body: Joi.array().items(
    Joi.object().keys({
      answer: Joi.string().required(),
      question: Joi.required().custom(objectId),
      allocation: Joi.required().custom(objectId),
    })
  ),
};

const getAnswers = {
  query: Joi.object().keys({
    question: Joi.required().custom(objectId),
  }),
};

const getAllocationAnswers = {
  query: Joi.object().keys({
    allocationId: Joi.required().custom(objectId),
  }),
};

module.exports = {
  createAnswers,
  getAnswers,
  getAllocationAnswers,
};
