const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createHealthEmergency = {
  body: Joi.object().keys({
    status: Joi.string().required().valid("safe", "unsafe"),
  }),
};

const getHealthEmergency = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    getAll: Joi.boolean(),
    dept: Joi.custom(objectId),
  }),
};

module.exports = {
  createHealthEmergency,
  getHealthEmergency,
};
