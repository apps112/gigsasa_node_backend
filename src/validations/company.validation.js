const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createCompany = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    team_size: Joi.string().required(),
    phone: Joi.string().required(),
    industry: Joi.string().required(),
    performance_management: Joi.boolean(),
    payroll: Joi.boolean(),
    image_recognition: Joi.boolean(),
    attendance: Joi.boolean(),
    invite_email: Joi.string(),
  }),
};

const getCompany = {
  query: Joi.object().keys({
    companyId: Joi.required().custom(objectId),
  }),
};

const updateCompany = {
  params: Joi.object().keys({
    companyId: Joi.required().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string(),
    team_size: Joi.string(),
    phone: Joi.string(),
    industry: Joi.string(),
    performance_management: Joi.boolean(),
    payroll: Joi.boolean(),
    image_recognition: Joi.boolean(),
    leave_management: Joi.boolean(),
    attendance: Joi.boolean(),
  }),
};

module.exports = {
  createCompany,
  getCompany,
  updateCompany,
};
