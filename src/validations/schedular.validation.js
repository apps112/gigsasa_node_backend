const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createSchedular = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    users: Joi.array().items(Joi.required().custom(objectId)),
    creator: Joi.required().custom(objectId),
    company: Joi.required().custom(objectId),
    monday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    tuesday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    wednesday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    thursday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    friday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    saturday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
    sunday: Joi.object().keys({
      start: Joi.string(),
      end: Joi.string(),
    }),
  }),
};

const updateSchedular = {
  params: Joi.object().keys({
    schedularId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      users: Joi.array().items(Joi.required().custom(objectId)),
      creator: Joi.required().custom(objectId),
      company: Joi.required().custom(objectId),
      monday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      tuesday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      wednesday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      thursday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      friday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      saturday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
      sunday: Joi.object().keys({
        start: Joi.string(),
        end: Joi.string(),
      }),
    })
    .min(1),
};

const getSchedular = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const deleteSchedular = {
  body: Joi.object().keys({
    id: Joi.required().custom(objectId),
  }),
};

module.exports = {
  createSchedular,
  getSchedular,
  updateSchedular,
  deleteSchedular,
};
