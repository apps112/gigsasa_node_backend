const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createUser = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    name: Joi.string().required(),
    role: Joi.string().required().valid("user", "admin", "supervisor"),
    phone: Joi.string().required(),
    jobType: Joi.string().required(),
    organization: Joi.string().custom(objectId),
    company_id: Joi.string().custom(objectId),
  }),
};

const createMultipleUsers = {
  body: Joi.array().items(
    Joi.object().keys({
      email: Joi.string().required().email(),
      name: Joi.string().required(),
      role: Joi.string().required().valid("user", "admin", "supervisor"),
      phone: Joi.string().required(),
      jobType: Joi.string().required(),
      organization: Joi.string().custom(objectId),
      company_id: Joi.required().custom(objectId),
    })
  ),
};

const getUsers = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    getAll: Joi.boolean(),
    dept: Joi.string().custom(objectId),
  }),
};

const getUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

const updateUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      role: Joi.string().valid("user", "admin", "supervisor"),
      organization: Joi.string().custom(objectId),
      jobType: Joi.string(),
      active: Joi.boolean(),
      password: Joi.string(),
      isInitialPassword: Joi.boolean(),
    })
    .min(1),
};

const updateProfile = {
  body: Joi.object().keys({
    name: Joi.string(),
    profilePic: Joi.string(),
  }),
};

const deleteUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
};

const joinRequest = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
  }),
};

const tourStatus = {
  body: Joi.object().keys({
    status: Joi.string().required(),
    userId: Joi.required().custom(objectId),
  }),
};

const joinUser = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
};

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  createMultipleUsers,
  updateProfile,
  joinUser,
  joinRequest,
  tourStatus,
};
