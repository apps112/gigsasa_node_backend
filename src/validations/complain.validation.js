const Joi = require("joi");

const createComplain = {
  body: Joi.object().keys({
    comment: Joi.string().required(),
    email: Joi.string().required().email(),
    name: Joi.string().required(),
    subject: Joi.string().required(),
  }),
};

module.exports = {
  createComplain,
};
