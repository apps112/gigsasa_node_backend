const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createShift = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    users: Joi.array().items(Joi.required().custom(objectId)),
    start: Joi.date().required(),
    end: Joi.date().required(),
    location: Joi.required().custom(objectId),
    isRepeat: Joi.boolean().required(),
    repeatUntil: Joi.date(),
    days: Joi.array().items(Joi.date()),
    minimumHours: Joi.number().required(),
  }),
};

const getAllocations = {
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
    dept: Joi.custom(objectId),
  }),
};

const getUserAllocations = {
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
  }),
};

const deleteAllocation = {
  params: Joi.object().keys({
    allocationId: Joi.required().custom(objectId),
  }),
};

const updateShift = {
  params: Joi.object().keys({
    shiftId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      users: Joi.array().items(Joi.custom(objectId)),
      start: Joi.date(),
      end: Joi.date(),
      location: Joi.custom(objectId),
      isRepeat: Joi.boolean(),
      repeatUntil: Joi.date(),
      days: Joi.array().items(Joi.date()),
    })
    .min(1),
};

const clockIn = {
  body: Joi.object().keys({
    allocation: Joi.custom(objectId),
    clockin: Joi.date().required(),
    clockinImage: Joi.string(),
    clockinCoord: Joi.object().keys({
      latitude: Joi.number(),
      longitude: Joi.number(),
    }),
    clockInByAdmin: Joi.custom(objectId),
    customer: Joi.custom(objectId),
    answers: Joi.array().items(
      Joi.object().keys({
        answer: Joi.string(),
        question: Joi.custom(objectId),
        allocation: Joi.custom(objectId),
      })
    ),
  }),
};

const breakIn = {
  body: Joi.object().keys({
    allocation: Joi.required().custom(objectId),
    in: Joi.date().required(),
    inImage: Joi.string(),
    inCoord: Joi.object().keys({
      latitude: Joi.number(),
      longitude: Joi.number(),
    }),
    reason: Joi.string(),
    inByAdmin: Joi.custom(objectId),
  }),
};

const breakOut = {
  body: Joi.object().keys({
    allocation: Joi.required().custom(objectId),
    out: Joi.date().required(),
    outImage: Joi.string(),
    outCoord: Joi.object().keys({
      latitude: Joi.number(),
      longitude: Joi.number(),
    }),
    outByAdmin: Joi.custom(objectId),
  }),
};

const clockOut = {
  body: Joi.object().keys({
    allocation: Joi.required().custom(objectId),
    clockout: Joi.date().required(),
    clockoutImage: Joi.string(),
    clockoutCoord: Joi.object().keys({
      latitude: Joi.number(),
      longitude: Joi.number(),
    }),
    clockOutByAdmin: Joi.custom(objectId),
    answers: Joi.array().items(
      Joi.object().keys({
        answer: Joi.string(),
        question: Joi.custom(objectId),
        allocation: Joi.custom(objectId),
      })
    ),
  }),
};

const getCurrentStatus = {
  query: Joi.object().keys({
    page: Joi.string(),
    limit: Joi.string(),
    createAt: Joi.string(),
    dept: Joi.custom(objectId),
  }),
};

const getActivities = {
  query: Joi.object().keys({
    dept: Joi.custom(objectId),
  }),
};

const getUpcoming = {
  query: Joi.object().keys({
    page: Joi.number().required(),
  }),
};

module.exports = {
  createShift,
  getAllocations,
  updateShift,
  clockIn,
  breakIn,
  breakOut,
  clockOut,
  getCurrentStatus,
  deleteAllocation,
  getUpcoming,
  getActivities,
  getUserAllocations,
};
