const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createTimesheet = {
  body: Joi.array().items(
    Joi.object().keys({
      clockin: Joi.date(),
      clockout: Joi.date(),
      date: Joi.date(),
      status: Joi.string(),
      breaks: Joi.array().items(
        Joi.object().keys({
          in: Joi.date(),
          out: Joi.date(),
          reason: Joi.string(),
        })
      ),
      user: Joi.required().custom(objectId),
    })
  ),
};

const requestTimesheet = {
  body: Joi.array().items(
    Joi.object().keys({
      clockin: Joi.date(),
      clockout: Joi.date(),
      date: Joi.date(),
      status: Joi.string(),
      breaks: Joi.array().items(
        Joi.object().keys({
          in: Joi.date(),
          out: Joi.date(),
          reason: Joi.string(),
        })
      ),
      user: Joi.required().custom(objectId),
    })
  ),
};

const updateRequest = {
  body: Joi.object().keys({
    clockin: Joi.date(),
    clockout: Joi.date(),
    date: Joi.date(),
    status: Joi.string(),
    breaks: Joi.array().items(
      Joi.object().keys({
        in: Joi.date(),
        out: Joi.date(),
        reason: Joi.string(),
      })
    ),
    user: Joi.required().custom(objectId),
  }),
  query: {
    allocation: Joi.required().custom(objectId),
    timesheets: Joi.array().items(Joi.required().custom(objectId)),
  },
};

const getRequests = {
  query: Joi.object().keys({
    page: Joi.number(),
  }),
};

const getTimesheets = {
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
    page: Joi.number(),
    dept: Joi.custom(objectId),
  }),
};

const deleteTimesheet = {
  body: Joi.object().keys({
    timesheets: Joi.array().items(Joi.required().custom(objectId)),
    allocation: Joi.required().custom(objectId),
  }),
};

const getPending = {
  query: Joi.object().keys({
    page: Joi.number(),
    dept: Joi.custom(objectId),
  }),
};

const updateStatus = {
  body: Joi.object().keys({
    timesheets: Joi.array().items(Joi.required().custom(objectId)),
    status: Joi.string().required(),
  }),
};

const getDailyTimesheets = {
  query: Joi.object().keys({
    page: Joi.number().required(),
    dept: Joi.custom(objectId),
  }),
};

const getUserTimesheets = {
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
  }),
};

const getSpecificTimesheets = {
  body: {
    id: Joi.required().custom(objectId),
  },
  query: Joi.object().keys({
    start: Joi.date().required(),
    end: Joi.date().required(),
    page: Joi.number(),
  }),
};

const getTimeSheetTotal = {
  body: {
    id: Joi.custom(objectId),
  },
  query: Joi.object().keys({
    start: Joi.date(),
    end: Joi.date(),
    queryType: Joi.string().required(),
    dept: Joi.custom(objectId),
  }),
};

const editTime = {
  body: {
    allocation: Joi.required().custom(objectId),
    timesheets: Joi.array().items(Joi.required().custom(objectId)),
    clockin: Joi.date().required(),
    clockout: Joi.date().required(),
    activity: Joi.custom(objectId),
  },
};

module.exports = {
  createTimesheet,
  requestTimesheet,
  getTimesheets,
  getPending,
  getUserTimesheets,
  getDailyTimesheets,
  getSpecificTimesheets,
  getTimeSheetTotal,
  editTime,
  getRequests,
  updateRequest,
  updateStatus,
  deleteTimesheet,
};
