const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createTimer = {
  body: Joi.object().keys({
    inTime: Joi.date().required(),
    allocation: Joi.required().custom(objectId),
  }),
};

const saveBreak = {
  body: Joi.object().keys({
    breakTime: Joi.date().required(),
    allocation: Joi.required().custom(objectId),
    counter: Joi.string().required(),
  }),
};

const saveBreakout = {
  body: Joi.object().keys({
    breaks: Joi.number().required(),
    allocation: Joi.required().custom(objectId),
  }),
};

module.exports = {
  createTimer,
  saveBreak,
  saveBreakout,
};
